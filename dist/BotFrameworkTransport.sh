#!/bin/bash
export PATH=/usr/lib/jvm/java-8-oracle/jre/bin/:$PATH
err=1
until [ $err == 0 ];
do
	[ -f log/java0.log.0 ] && mv log/java0.log.0 "log/`date +%Y-%m-%d_%H-%M-%S`_java.log"
	[ -f log/stdout.log ] && mv log/stdout.log "log/`date +%Y-%m-%d_%H-%M-%S`_stdout.log"
	java -Djava.net.preferIPv4Stack=true -Dfile.encoding=UTF-8 -cp ./BotFrameworkTransport.jar ru.tro.botframework4xmpp.Server > log/stdout.log 2>&1
	err=$?
	sleep 10;
done
