package clientapi.okruclient;

import clientapi.okruclient.model.message.OkRuMessageBean;
import clientapi.okruclient.model.subscribe.Subscription;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by aleksey on 07.05.17.
 */
public class OkRuClient {
    private static final Logger _log = Logger.getLogger(OkRuClient.class.getName());

    private String token;

    public OkRuClient(String token) {
        this.token = token;
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public void sendMessage(OkRuMessageBean msg) throws IOException {
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(msg));
        Request request = new Request.Builder()
                .url(OkRuMessageBean.OUTPOST_URL + token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());

    }

    public void subscribe(String url) throws IOException {
        Subscription subscription = new Subscription(url);
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(subscription));
        Request request = new Request.Builder()
                .url(Subscription.REG_URL + token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        JsonObject answer = new JsonParser().parse(response.body().string()).getAsJsonObject();
        _log.info(answer.toString());
        if (answer.has("error_message")) {

            throw new IOException(answer.toString());
        }
    }

    public void unsubscribe(String url) throws IOException {
        Subscription subscription = new Subscription(url);
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(subscription));
        Request request = new Request.Builder()
                .url(Subscription.UNREG_URL + token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());

    }
}
