package clientapi.okruclient.model.subscribe;

/**
 * Created by aleksey on 07.05.17.
 */
public class Subscription {

    public static final String REG_URL = "https://api.ok.ru/graph/me/subscribe?access_token=";
    public static final String UNREG_URL = "https://api.ok.ru/graph/me/unsubscribe?access_token=";

    private String url;

    public Subscription(String url) {
        this.url = url;
    }
}
