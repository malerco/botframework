package clientapi.okruclient.model.message;

/**
 * Created by aleksey on 07.05.17.
 */
public class Message {
    private String text;
    private Attachment attachment;
    private Attachment[] attachments;

    public Message(String body) {
        text = body;
    }

    public String getText() {
        return text;
    }

    public Attachment[] getAttachments() {
        return attachments;
    }
}
