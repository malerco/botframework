package clientapi.okruclient.model.message;

/**
 * Created by aleksey on 07.05.17.
 */
public class Recipient {
    private String chat_id;

    Recipient(String chatId) {
        chat_id = chatId;
    }

    public String getChatId() {
        return chat_id;
    }
}
