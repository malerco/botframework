package clientapi.okruclient.model.message;


/**
 * Created by aleksey on 07.05.17.
 */
public class OkRuMessageBean {
    public static final String OUTPOST_URL = "https://api.ok.ru/graph/me/messages?access_token=";

    private String owner_jid;
    private Recipient recipient;
    private Message message;
    private Sender sender;

    public OkRuMessageBean(String chatId, String body) {
        recipient = new Recipient(chatId);
        message = new Message(body);
    }


    public Recipient getRecipient() {
        return recipient;
    }

    public Message getMessage() {
        return message;
    }

    public Sender getSender() {
        return sender;
    }

    public String getOwnerJid() {
        return owner_jid;
    }
}
