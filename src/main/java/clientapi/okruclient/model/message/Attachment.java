package clientapi.okruclient.model.message;

/**
 * Created by aleksey on 07.05.17.
 */
public class Attachment {
    private String type;
    private Payload payload;

    public String getType() {
        return type;
    }

    public Payload getPayload() {
        return payload;
    }
}
