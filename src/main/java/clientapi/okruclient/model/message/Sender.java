package clientapi.okruclient.model.message;

/**
 * Created by aleksey on 07.05.17.
 */
public class Sender {
    private String user_id;
    private String name;

    public String getUserId() {
        return user_id;
    }

    public String getName() {
        return name;
    }
}
