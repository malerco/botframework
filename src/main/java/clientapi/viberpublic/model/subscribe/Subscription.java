package clientapi.viberpublic.model.subscribe;

/**
 * Created by aleksey on 07.05.17.
 */
public class Subscription {

    public static final String REG_URL = "https://chatapi.viber.com/pa/set_webhook";

    private String url;

    public Subscription(String url) {
        this.url = url;
    }
}
