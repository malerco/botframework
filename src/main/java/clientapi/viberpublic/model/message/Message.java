package clientapi.viberpublic.model.message;

/**
 * Created by aleksey on 07.05.17.
 */
public class Message {
    public enum Type {text, picture, video, file, sticker, contact, url}

    private String text;
    private String media;
    private Type type;
    private String receiver;

    public Message (String contacdId, String text) {
        receiver = contacdId;
        this.text = text;
        type = Type.text;
    }

    public Message(String message) {
        this.text = message;
    }

    public String getText() {
        return text;
    }

    public String getMedia() {
        return media;
    }

    public Type getType() {
        return type;
    }
}
