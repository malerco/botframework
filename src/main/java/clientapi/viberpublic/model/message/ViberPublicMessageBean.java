package clientapi.viberpublic.model.message;

public class ViberPublicMessageBean {
    private Sender sender;
    private Message message;
    private String owner_jid;


    public Sender getSender() {
        return sender;
    }

    public Message getMessage() {
        return message;
    }

    public String getOwner_jid() {
        return owner_jid;
    }
}
