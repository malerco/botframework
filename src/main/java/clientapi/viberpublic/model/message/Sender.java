package clientapi.viberpublic.model.message;

public class Sender {
    private String id;
    private String name;

    public Sender(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
