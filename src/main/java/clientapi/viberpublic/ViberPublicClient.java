package clientapi.viberpublic;

import clientapi.viberpublic.model.message.Message;
import clientapi.viberpublic.model.subscribe.Subscription;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by aleksey on 07.05.17.
 */
public class ViberPublicClient {
    private static final Logger _log = Logger.getLogger(ViberPublicClient.class.getName());
    private static final String OUTPOST_URL = "https://chatapi.viber.com/pa/send_message";
    private String token;

    public ViberPublicClient(String token) {
        this.token = token;
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public void sendMessage(Message msg) throws IOException {
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(msg));
        Request request = new Request.Builder()
                .url(OUTPOST_URL)
                .addHeader("X-Viber-Auth-Token", token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());

    }

    public void subscribe(String url) throws IOException {
        Subscription subscription = new Subscription(url);
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(subscription));
        Request request = new Request.Builder()
                .url(Subscription.REG_URL)
                .addHeader("X-Viber-Auth-Token", token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        JsonObject answer = new JsonParser().parse(response.body().string()).getAsJsonObject();
        _log.info(answer.toString());
        if (answer.has("error_message")) {

            throw new IOException(answer.toString());
        }
    }

    public void unsubscribe() throws IOException {
        Subscription subscription = new Subscription("");
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(subscription));
        Request request = new Request.Builder()
                .url(Subscription.REG_URL)
                .addHeader("X-Viber-Auth-Token", token)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());

    }
}
