package clientapi.wechatclient;

import clientapi.wechatclient.model.AccessToken;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.io.IOException;

/**
 * Created by ivametal on 16/06/17.
 */
public class WeChatTokenExtender extends Thread {
    private UserBean owner;
    private OkHttpClient client = new OkHttpClient();

    public WeChatTokenExtender(UserBean owner) {
        this.owner = owner;
    }



    @Override
    public void run() {
        long lastUpdate = 0;
        long expiresIn = 7200000;
        while (true) {
            try {
                Thread.sleep(1000);
                if (lastUpdate+expiresIn <= System.currentTimeMillis()+10000) {
                    String urlWithData = "https://api.wechat.com/cgi-bin/token?grant_type=client_credential&appid="+
                            owner.getAppId()+"&secret="+owner.getAppSecret();

                    Request request = new Request.Builder()
                            .url(urlWithData)
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        AccessToken accessToken = new Gson().fromJson(response.body().string(), AccessToken.class);
                        owner.setAccessToken(accessToken.getAccessToken());
                        expiresIn = Integer.valueOf(accessToken.getExpiresIn())*1000;
                        lastUpdate = System.currentTimeMillis();
                    } catch (IOException e) {
                        e.printStackTrace();

                        XMPPMessage.sendWarning(owner.getChannel(), "Error while refreshing token from WeChatConnection for user " + owner.getJid() + "\n" +
                                e.getMessage());
                        Thread.sleep(20000);
                    }
                }
                else  {
                    try {
                        Thread.sleep((lastUpdate + expiresIn) - System.currentTimeMillis() - 10000);
                    }
                    catch (InterruptedException exc) {
                        exc.printStackTrace();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
