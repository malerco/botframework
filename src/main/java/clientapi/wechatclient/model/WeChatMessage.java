package clientapi.wechatclient.model;


import clientapi.wechatclient.model.type.Text;

/**
 * Created by ivametal on 14/06/17.
 */
public class WeChatMessage {
    private String owner_jid;
    private String fromuser;
    private String touser;
    private MessageType msgtype;
    private Text text;

    public WeChatMessage(String toUser, MessageType type, String content) {
        this.touser = toUser;
        this.msgtype = type;
        if (type == MessageType.text)
            this.text = new Text(content);
    }

    public String getContent() {
        return text.getContent();
    }

    public MessageType getContentType() {
        return msgtype;
    }

    public String getTouser() {
        return touser;
    }



    public String getFromuser() {
        return fromuser;
    }

    public String getOwner_jid() {
        return owner_jid;
    }

    public enum MessageType {text, image}
}
