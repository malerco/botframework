package clientapi.wechatclient.model;

/**
 * Created by ivametal on 16/06/17.
 */
public class AccessToken {
    private String access_token;
    private String expires_in;

    public String getAccessToken() {
        return access_token;
    }

    public String getExpiresIn() {
        return expires_in;
    }
}
