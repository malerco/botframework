package clientapi.wechatclient.model.type;

/**
 * Created by ivametal on 16/06/17.
 */
public class Text {
    private String content;

    public Text(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
