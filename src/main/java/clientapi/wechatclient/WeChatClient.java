package clientapi.wechatclient;

import clientapi.wechatclient.model.WeChatMessage;
import com.google.gson.Gson;
import com.squareup.okhttp.*;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by ivametal on 14/06/17.
 */
public class WeChatClient {
    private static Logger _log = Logger.getLogger(WeChatClient.class.getName());

    private String token;
    private OkHttpClient client;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public WeChatClient(String token) {
        this.token = token;
        client = new OkHttpClient();
    }

    public void sendMessage(WeChatMessage message) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON, new Gson().toJson(message));
        Request request = new Request.Builder()
                .url("https://api.wechat.com/cgi-bin/message/custom/send?access_token="+token)
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());
    }
}
