package clientapi.telegrambotclient.model;

/**
 * Created by BeS on 03.12.2017.
 */

public class Result {
    String file_id;
    String file_size;
    String file_path;

    public Result() {
    }

    public Result(String file_id, String file_size, String file_path) {
        this.file_id = file_id;
        this.file_size = file_size;
        this.file_path = file_path;
    }

    public String getFile_id() {
        return file_id;
    }

    public String getFile_size() {
        return file_size;
    }

    public String getFile_path() {
        return file_path;
    }
}
