package clientapi.telegrambotclient.model;

/**
 * Created by BeS on 03.12.2017.
 */

public class Response {
    String ok;
    Result result;

    public Response() {
    }

    public Response(String ok, Result result) {
        this.ok = ok;
        this.result = result;
    }

    public String getOk() {
        return ok;
    }

    public Result getResult() {
        return result;
    }
}
