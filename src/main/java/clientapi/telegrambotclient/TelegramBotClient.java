package clientapi.telegrambotclient;


import com.github.badoualy.telegram.api.TelegramClient;
import com.github.badoualy.telegram.tl.api.TLInputPeerUser;
import com.github.badoualy.telegram.tl.exception.RpcErrorException;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;

import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.network.TelegramConnection;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Created by BeS on 29.11.2017.
 */

public class TelegramBotClient extends TelegramLongPollingBot {

    private UserBean owner;
    private Message message;
    public TelegramBotClient(UserBean owner){
        this.owner = owner;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            message = update.getMessage();
            System.out.print("Message: " + message.getText());
            System.out.print("Chat ID: " + message.getChatId());
            message = update.getMessage();
            ContactBean contactBean = getContact(message);
            //отправляем сообщение
            XMPPMessage.sendMessageFromTelegramBot(owner, contactBean.getJid(), message, getBotToken());
        }
    }

    @Override
    public String getBotUsername() {
        return " ";
    }

    @Override
    public String getBotToken() {
        return owner.getAccessToken();
    }

    public void sendMesaageToUser(SendMessage message) throws TelegramApiException {
        execute(message);
    }

    private ContactBean getContact(Message message) {
        int userId = message.getFrom().getId();
        ContactBean contactBean = Contacts.getInstance().findById(owner, userId);

        if (contactBean == null) {
            contactBean = new ContactBean(userId + Server.CHANNEL.telegram3.getWithAt(),
                    owner.getJid(),
                    owner.getId(), userId+"",
                    (message.getChat().getFirstName() != null ? message.getChat().getFirstName() + " " : "") +  (message.getChat().getLastName() != null ? message.getChat().getLastName() : ""),
                    userId+"",
                    Server.CHANNEL.telegram3);
            Contacts.getInstance().tryInsert(Server.CHANNEL.telegram3, contactBean);
        }
        return contactBean;
    }
}
