package clientapi.botframeworkclient.model;

import ru.tro.botframework4xmpp.beans.ConversationBean;

/**
 * Created by aleksey on 04.03.17.
 */
public class BotFMessageBean {
    private String owner_jid;
    private String text;
    private String type;
    private String channelId;
    private ConversationBean conversation;
    private ChannelAccount from;
    private ChannelAccount recipient;
    private Attachment attachments[];
    private String serviceUrl;

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    public String getChannelId() {
        return channelId;
    }

    public ConversationBean getConversation() {
        return conversation;
    }

    public ChannelAccount getFrom() {
        return from;
    }

    public ChannelAccount getRecipient() {
        return recipient;
    }

    public Attachment[] getAttachments() {
        return attachments;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public String getOwnerJid() {
        return owner_jid;
    }

}
