package clientapi.vkclient;

import clientapi.okruclient.model.message.OkRuMessageBean;
import clientapi.okruclient.model.subscribe.Subscription;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by aleksey on 07.05.17.
 */
public class VkClient {
    private static final Logger _log = Logger.getLogger(VkClient.class.getName());

    private String token;

    public VkClient(String token) {
        this.token = token;
    }

    private OkHttpClient client = new OkHttpClient();

    public void sendMessage(Request request) throws IOException {
        Response response = client.newCall(request).execute();
        _log.info(response.body().string());
    }
}
