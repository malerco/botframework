package clientapi.vkclient.model;

import clientapi.vkclient.model.attachments.Attachment;

/**
 * Created by aleksey on 20.05.17.
 */
public class VkMessage {

    private long id;
    private long date;
    private int out;
    private String user_id;
    private int read_state;
    private String title;
    private String body;
    private Attachment[] attachments;

    public long getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public boolean isOut() {
        return out == 1;
    }

    public String getUserId() {
        return user_id;
    }

    public int getReadState() {
        return read_state;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Attachment[] getAttachments() {
        return attachments;
    }
}
