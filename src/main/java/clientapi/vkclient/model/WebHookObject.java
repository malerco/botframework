package clientapi.vkclient.model;

/**
 * Created by aleksey on 20.05.17.
 */
public class WebHookObject {
    public enum Type {message_new}

    private String channelId;
    private String owner_jid;
    private Type type;
    private long group_id;
    private VkMessage object;

    public VkMessage getMessage() {
        return object;
    }

    public String getChannel() {
        return channelId;
    }

    public String getOwnerJid() {
        return owner_jid;
    }
}
