package clientapi.vkclient.model.attachments;

/**
 * Created by aleksey on 20.05.17.
 */
public class Photo {

    private String photo_75;
    private String photo_130;
    private String photo_604;
    private String photo_807;
    private String photo_1280;
    private String photo_2560;

    public String getUrl() {
        if (photo_2560 != null) {
            return photo_2560;
        } else if (photo_1280 != null) {
            return photo_1280;
        } else if (photo_807 != null) {
            return photo_807;
        } else if (photo_604 != null) {
            return photo_604;
        } else if (photo_130 != null) {
            return photo_130;
        } else if (photo_75 != null) {
            return photo_75;
        } else return "";
    }
}
