package clientapi.vkclient.model.attachments;

/**
 * Created by aleksey on 20.05.17.
 */
public class Attachment {

    public enum Type {photo, doc, link}

    private Type type;
    private Photo photo;
    private Doc doc;
    private Link link;

    public Type getType() {
        return type;
    }

    public Photo getPhoto() {
        return photo;
    }

    public Doc getDoc() {
        return doc;
    }

    public Link getLink() {
        return link;
    }

}
