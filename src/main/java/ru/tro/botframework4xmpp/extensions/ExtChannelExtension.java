package ru.tro.botframework4xmpp.extensions;

import org.xmpp.packet.PacketExtension;

/**
 * Created by vishnyakov on 13.06.17.
 */
public class ExtChannelExtension extends PacketExtension {

    public static final String NAME = "extchannel";
    public static final String NAMESPACE = "wiki/Extchannel";
    public ExtChannelExtension(String phone) {
        super(NAME, NAMESPACE);
        this.getElement().addText(phone);
    }
}
