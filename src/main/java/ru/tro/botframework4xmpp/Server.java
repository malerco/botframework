package ru.tro.botframework4xmpp;


import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;



import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.singleton.ApiSocketServer;
import ru.tro.botframework4xmpp.singleton.WebhookSocketServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by aleksey on 29.01.17.
 */
public class Server {
    private static final Logger _log = Logger.getLogger(Server.class.getName());

    private static final String LOG_NAME = "./log.cfg";

    private static final String LOG_FOLDER = "log";

    public enum CHANNEL {skype, facebook, slack, kik, okru, vk, wechat, fb, telegram2, viberpublic, telegram3;

        public String getWithDomain() {
            return this.name() + "." + Config.XMPP_HOST;
        }

        public String getWithAt() {
            return "@" + this.name() + "." + Config.XMPP_HOST;
        }

        public String getWithResource(String jid) {
            return jid + "@" + this.name() + "." + Config.XMPP_HOST + "/bot";
        }
    }

    public static void main(String[] args) {

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.exit(1);
            }
        }, (20+new Random().nextInt(20))*60*1000);
        // Create log folder
        File logFolder = new File("./", LOG_FOLDER);
        logFolder.mkdir();

        // Create input stream for log file -- or store file data into memory
        try (InputStream is = new FileInputStream(new File(LOG_NAME)))
        {
            LogManager.getLogManager().readConfiguration(is);
        }
        catch (Exception e)
        {
            _log.info("Exception in Server.main(): " + e.getMessage());
        }

        printSection("Config");
        Config.load();
        printSection("EndConfig");

        ApiContextInitializer.init();

        printSection("Services");
        DatabaseManager.getInstance();
        _log.info("Openfire gateway started.");
        ChannelSwitcher.getInstance().init();
        WebhookSocketServer.getInstance();
        ApiSocketServer.getInstance();
        printSection("EndServices");

        printSection("PID");
        _log.info("Server start with PID: " + getPid());
        printSection("EndPID");



        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            printSection("Shutdown server...");
            System.out.println("Shutdown server....");
            try
            {
                WebhookSocketServer.getInstance().stopSocketServer();
                ApiSocketServer.getInstance().stopSocketServer();
                ChannelSwitcher.getInstance().shutdown();
                Thread.sleep(3000);
            }
            catch (Exception ignored)
            {

            }
        }));
    }

    public static void printSection(String s) {
        s = "=[ " + s + " ]";
        while (s.length() < 56) {
            s = "-" + s;
        }
        _log.info(s);
    }

    private static String getPid()
    {
        String pid = "-1";
        byte[] bo = new byte[256];
        try (InputStream is = new FileInputStream("/proc/self/stat"))
        {
            try
            {
                is.read(bo);
            }
            catch (IOException e)
            {

            }
            for (int i = 0; i < bo.length; i++)
            {
                if ((bo[i] < '0') || (bo[i] > '9'))
                {
                    pid =  new String(bo, 0, i);
                }
            }
        }
        catch (Exception e)
        {

        }

        if (pid.equals("-1"))
        {
            try
            {
                String prName = ManagementFactory.getRuntimeMXBean().getName();
                int index = prName.indexOf('@');

                if (index >= 1)
                {
                    pid = prName.substring(0, index);
                }
            }
            catch (Exception ex)
            {

            }
        }

        return pid;
    }
}
