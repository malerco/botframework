package ru.tro.botframework4xmpp.packets;

import clientapi.okruclient.model.message.OkRuMessageBean;
import clientapi.telegrambotclient.model.*;
import clientapi.viberpublic.model.message.ViberPublicMessageBean;
import clientapi.vkclient.model.VkMessage;
import clientapi.wechatclient.model.WeChatMessage;

import com.google.gson.Gson;
import com.restfb.types.webhook.messaging.MessagingAttachment;
import com.restfb.types.webhook.messaging.MessagingItem;
import com.squareup.okhttp.*;
import com.squareup.okhttp.Response;

import clientapi.botframeworkclient.model.Attachment;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.MessageEntity;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.xmpp.packet.PacketExtension;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import clientapi.botframeworkclient.model.BotFMessageBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.extensions.ExtChannelExtension;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.handlers.MessageGenerator;
import ru.tro.botframework4xmpp.network.TelegramConnection;
import ru.tro.botframework4xmpp.network.UploadHandler;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.TelegramBot;
import ru.tro.botframework4xmpp.singleton.Users;
import sun.rmi.runtime.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обработчик пакетов типа VkMessage
 * @author TANT
 *
 */
public class XMPPMessage {

	private static final Logger _log = Logger.getLogger(XMPPMessage.class.getName());
	
	public static void process(Packet packet) {

		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}

		Message msg = (Message) packet;
		
		if (msg.getBody() != null)
		{
            UserBean user = null;
            PacketExtension extChannelExtension = msg.getExtension(ExtChannelExtension.NAME, ExtChannelExtension.NAMESPACE);

            //инстанс пользователя от кого нужно отправить сообщение
            if (packet.getTo().toString().equals(Server.CHANNEL.telegram2.getWithDomain()) &&
                    msg.getBody().startsWith("+")) {
                String[] credentials = msg.getBody().split(" ");
                if (credentials.length != 2) {
                    Packet authMessage = MessageGenerator.genMsg(packet.getFrom().toString(),
                            Server.CHANNEL.telegram2.getWithDomain(), "Wrong credentials");
                    ChannelSwitcher.getInstance().getComponent(Server.CHANNEL.telegram2).sendPacket(authMessage);
                }
                user = Users.getInstance().findUserByLogin(CHNL, credentials[0]);
                ((TelegramConnection) user.getConnection())
                        .sendCode(credentials[1]);
                return;
            } else if (extChannelExtension == null) {
                user = Users.getInstance().findUserByJid(CHNL, msg.getFrom().toBareJID());
            } else {
                user = Users.getInstance().findUserByLogin(CHNL, extChannelExtension.getElement().getText());
            }


			String to = packet.getTo().toBareJID();
            ContactBean contact = Contacts.getInstance().findByJid(to);
			if (user != null && contact != null)
			{
				String message = msg.getBody().replaceAll("\\r|\\n", " ");
				if (Config.DEBUG)
				{
					System.out.println("Send message from="+user.getJid()+" to="+to+" from AppID="+user.getAppId()+" msg="+message);
				}
				if (Config.JSON_LOG)
				{
					_log.info("send - {\"status\":\"send\",\"from jid\":\""+user.getJid()+"\",\"msg\":\""+message+"\"}");
				}


				//отправка сообщения в апи
				user.getConnection().sendMessage(contact, message);


			}
			else {
				//XMPPGateway.component.sendPacket(MessageGenerator.genMsg(msg.getFrom(), msg.getTo(), Config.USER_OFFLINE_MSG));
			}
		}
		
	}

    //single message from BotFramework API
    public static void sendMessageFromBotFramework(UserBean user, String from, BotFMessageBean message) {
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.getAttachments() != null) {
            for (Attachment attachment : message.getAttachments()) {

                try {
                    //качаем фото
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(attachment.getContentUrl())
                            .addHeader("Authorization", "Bearer " + user.getAccessToken())
                            .addHeader("Content-Type", attachment.getContentType())
                            .build();
                    Response response = client.newCall(request).execute();

                    InputStream in = response.body().byteStream();

                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                    int nRead;
                    byte[] data = new byte[16384];

                    while ((nRead = in.read(data, 0, data.length)) != -1) {
                        buffer.write(data, 0, nRead);
                    }
                    buffer.flush();
                    response.body().close();

                    //заливаем фото на наш сервер
                    String link = new UploadHandler().upload(attachment.getName(), attachment.getContentType(), buffer);
                    packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, link));

                } catch (Exception e) {
                    e.printStackTrace();
                    XMPPMessage.sendWarning(user.getChannel(), "Error in 'sendMessageFromBotFramework' for user " + user.getJid() + "\n" +
                            e.getMessage());
                }
            }
        }
        if (message.getText() != null) {
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getText()));
        }
        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);

    }

    public static void sendMessageFromTelegramBot(UserBean user, String from, org.telegram.telegrambots.api.objects.Message message, String token){
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.hasPhoto()){
            String link = uploadFromTlgbotToServer(message.getPhoto().get(message.getPhoto().size() - 1).getFileId(), "image", token, user.getChannel(), user.getJid());
            if (link != null)
                packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, link + (message.getCaption() != null ? "\n" + message.getCaption() : "")));
        }
        if (message.hasEntities()) {
            for (MessageEntity entity : message.getEntities()){
                Server.printSection("\n" + "Entity Link: " + entity.getUrl());
                packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, entity.getUrl()));
            }
        }
        if (message.hasDocument()){
            Server.printSection("\n" + "Document Link: " + message.getDocument().getFileId());
            String link = uploadFromTlgbotToServer(message.getDocument().getFileId(), "text", token, user.getChannel(), user.getJid());
            if (link != null)
                packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, link + (message.getCaption() != null ? "\n" + message.getCaption() : "")));

        }
        if (message.hasLocation()){
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, "http://maps.google.com/maps?q="+message.getLocation().getLatitude() + ",+" + message.getLocation().getLongitude()));
        }
        if (message.hasText()){
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getText()));
        }
        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);
    }

    private static String uploadFromTlgbotToServer(String fileid, String type, String token, Server.CHANNEL channel, String jid){
        OkHttpClient client = new OkHttpClient();
        Request request;
        Response response;
        String link = null;

            try {
                //качаем фото
                request = new Request.Builder().url("https://api.telegram.org/bot" + token + "/getFile?file_id=" + fileid)
                        .build();
                clientapi.telegrambotclient.model.Response apiresponse = new Gson().fromJson(client.newCall(request).execute().body().string(), clientapi.telegrambotclient.model.Response.class);
                request = new Request.Builder().url("https://api.telegram.org/file/bot" + token + "/" + apiresponse.getResult().getFile_path())
                        .build();
                response = client.newCall(request).execute();
                InputStream in = response.body().byteStream();
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[16384];

                while ((nRead = in.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                response.body().close();
                //заливаем фото на наш сервер
                String filename = System.currentTimeMillis() + "telegrambot";
                String filepath = apiresponse.getResult().getFile_path();
                filename += filepath.substring(filepath.lastIndexOf("."), filepath.length());
                link = new UploadHandler().upload(filename, type, buffer);
            } catch (Exception e) {
                e.printStackTrace();
                XMPPMessage.sendWarning(channel, "Error in 'sendMessageFromTelegramBot' for user " + jid + "\n" +
                        e.getMessage());
            }
            return link;
    }

    public static void sendMessageFromOkRu(UserBean user, String from, OkRuMessageBean message) {
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.getMessage().getAttachments() != null) {
            for (clientapi.okruclient.model.message.Attachment attachment : message.getMessage().getAttachments()) {
                try {
                    //качаем фото
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(attachment.getPayload().getUrl())
                            .build();
                    Response response = client.newCall(request).execute();

                    InputStream in = response.body().byteStream();

                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                    int nRead;
                    byte[] data = new byte[16384];

                    while ((nRead = in.read(data, 0, data.length)) != -1) {
                        buffer.write(data, 0, nRead);
                    }
                    buffer.flush();
                    response.body().close();

                    //заливаем фото на наш сервер
                    String filename = System.currentTimeMillis() + "okru";
                    switch (attachment.getType()) {
                        case "image":
                            filename += ".png";
                            break;
                        case "video":
                            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, attachment.getPayload().getUrl()));
                            continue;
                    }
                    String link = new UploadHandler().upload(filename, attachment.getType(), buffer);
                    packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, link));

                } catch (Exception e) {
                    e.printStackTrace();
                    XMPPMessage.sendWarning(user.getChannel(), "Error in 'sendMessageFromOkRu' for user " + user.getJid() + "\n" +
                            e.getMessage());
                }

            }
        }
        if (message.getMessage().getText() != null) {
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getMessage().getText()));
        }
        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);
        System.out.print("From OKRU: " + packets.toString());
    }

    public static void sendMessageFromVk(UserBean user, String from, VkMessage message) {
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.getAttachments() != null) {
            for (clientapi.vkclient.model.attachments.Attachment attachment : message.getAttachments()) {
                String url = "[UNKNOWN ATTACHMENT]";
                switch (attachment.getType()) {
                    case photo:
                        url = attachment.getPhoto().getUrl();
                        break;
                    case doc:
                        url = attachment.getDoc().getUrl();
                        break;
                    case link:
                        url = attachment.getLink().getUrl();
                        break;
                }
                packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, url));
            }
        }
        if (message.getBody() != null && !message.getBody().isEmpty()) {
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getBody()));
        }
        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);
        System.out.print("From VK: " + packets.toString());

    }

    public static void sendWarning(Server.CHANNEL channel, String message) {
        TelegramBot.getInstance().sendBroadcast(message);

        if (channel != null) {
            ArrayList<Packet> packets = new ArrayList<>(Config.ADMINS_TO_REPORT.length);
            for (String jid : Config.ADMINS_TO_REPORT) {
                packets.add(MessageGenerator.genMsg(jid, channel.getWithDomain(), message));
            }
            ChannelSwitcher.getInstance().getComponent(channel).sendPackets(packets);
            System.out.print("From sendWarning: " + packets.toString());
        }
    }

    public static void sendMessageFromWechat(UserBean user, String contactJid, WeChatMessage message) {
	    Packet packet = MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), contactJid, message.getContent());
	    ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPacket(packet);
        System.out.print("From Wechat: " + packet.toString());
    }

    public static void sendMessageFromFB(UserBean user, String from, MessagingItem message) {
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.getMessage().getAttachments() != null) {
            for (MessagingAttachment attachment : message.getMessage().getAttachments()) {
                String url = attachment.getPayload().getUrl();

                packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, url));
            }
        }
        if (message.getMessage().getText() != null && !message.getMessage().getText().isEmpty()) {
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getMessage().getText()));
        }

        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);
        System.out.print("From Bot FB: " + packets.toString());
    }

    public static void sendMessageFromTelegram(UserBean owner, String jid, String message) {
        if (message != null && !message.isEmpty()) {
            Packet packet = MessageGenerator.genExtLoginMsg(owner.getLogin(), owner.getJid(), jid, message);
            ChannelSwitcher.getInstance().getComponent(owner.getChannel()).sendPacket(packet);
            System.out.print("From Telegram: " + packet.toString());
        }

    }


    public static void sendMessageFromViberPublic(UserBean user, String from, ViberPublicMessageBean message) {
        ArrayList<Packet> packets = new ArrayList<>();
        if (message.getMessage().getMedia() != null && !message.getMessage().getMedia().isEmpty()) {
            String url = message.getMessage().getMedia();
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, url));

        }
        if (message.getMessage().getText() != null && !message.getMessage().getText().isEmpty()) {
            packets.add(MessageGenerator.genExtLoginMsg(user.getLogin(), user.getJid(), from, message.getMessage().getText()));
        }

        ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packets);
        System.out.print("From Viber: " + packets.toString());
    }
}
