package ru.tro.botframework4xmpp.packets;

import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;
import org.xmpp.packet.PacketError;
import org.xmpp.packet.Presence;
import org.xmpp.packet.Presence.Type;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.handlers.PresenceHandler;
import ru.tro.botframework4xmpp.handlers.RosterHandler;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Обработчик входящих пакетов типа Presence
 * @author TANT
 *
 */
public class XMPPPresence {
	private static final Logger _log = Logger.getLogger(XMPPPresence.class.getName());

	public static void process(Packet packet) {
		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}

		Presence pre = (Presence) packet;
		UserBean user = Users.getInstance().findUserByJid(CHNL, pre.getFrom().toBareJID());
		if (user == null)
			return;

		if (pre.getPriority() == 50 || pre.getPriority() == 1) {
			//send all contacts
			ArrayList<Packet> contacts = new ArrayList<>();

			user.setOnline(true);
			user.setResource(packet.getFrom().getResource());


			for (ContactBean cont : Contacts.getInstance().findAllByOwner(user.getChannel(), user.getJid())) {
				contacts.add(RosterHandler.genRoster(user, cont.getJid(), cont.getName(), user.getChannel().name(), "to"));
				contacts.add(PresenceHandler.genStatus(user, cont.getJid(), null));
			}
			Packet ro = RosterHandler.genRoster(user, CHNL.getWithDomain(), CHNL.name(), "Transports", "from");
			contacts.add(PresenceHandler.genStatus(user, CHNL.getWithDomain(), null));
			contacts.add(ro);

			if (!contacts.isEmpty())
				ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(contacts);
		} else if (pre.getType() != null) {
			if (pre.getType() == Type.probe) {
				if (pre.getTo().toString().equals(user.getChannel().getWithDomain())) {
					ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPacket(PresenceHandler.genStatus(pre.getFrom(), pre.getTo(), null));
				}
			} else if (pre.getType() == Type.unavailable) {
				user.setOnline(false);

			}
		}

	}

}
