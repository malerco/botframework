package ru.tro.botframework4xmpp.packets;

import org.dom4j.Namespace;
import org.xmpp.packet.IQ;
import org.xmpp.packet.IQ.Type;
import org.xmpp.packet.Packet;
import ru.tro.botframework4xmpp.handlers.DiscoInfoHandler;
import ru.tro.botframework4xmpp.handlers.RegisterHandler;
import ru.tro.botframework4xmpp.handlers.ResultHandler;
import ru.tro.botframework4xmpp.handlers.RosterHandler;

/**
 * Обработчик входящих пакетов типа IQ
 * @author TANT
 *
 */
public class XMPPIQ {

	public static void process(Packet packet) {
		IQ iq = (IQ) packet;
		Namespace ns = null;

		if (iq.getType() == Type.get && packet.getElement().element("query") != null) {
			ns = packet
					.getElement()
					.element("query")
					.getNamespace();
			switch (ns.getURI()) {
				case "http://jabber.org/protocol/disco#info":
					DiscoInfoHandler.process(packet);
					break;
		
				case "jabber:iq:register":
					RegisterHandler.process(packet);
					break;
				default:
					break;
			}
		} else if (iq.getType() == Type.set) {
			ns = packet.getElement().element("query").getNamespace();
			switch (ns.getURI()) {
				case "jabber:iq:register":
					RegisterHandler.process(packet);
					break;
		
				case "jabber:iq:roster":
					RosterHandler.process(packet);
					break;
	
				default:
					break;
			}
		}
		else if (iq.getType() == Type.result)
		{
			ResultHandler.process(packet);
		}
		else if (iq.getType() == Type.error)
		{
			//TODO:
		}
		
	}

}
