package ru.tro.botframework4xmpp.beans;

import org.xmpp.packet.JID;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.network.*;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by aleksey on 29.01.17.
 */
public class UserBean {

    private int id;
    private String appId;
    private String appSecret;
    private String channelId;
    private String channelName = "";
    private String accessToken;
    private String phone;
    private String jid;
    private boolean isOnline;
    private String resource;
    private ApiConnection connection;
    private Server.CHANNEL channel;
    private String serviceUrl = "";

    public UserBean(Server.CHANNEL channel) {
        channelName = channel.name();
        this.channel = channel;
        switch (channel) {
            case facebook:
            case skype:
            case slack:
            case kik:
                connection = new BotFConnection(this);
                break;
            case okru:
                connection = new OkRuConnection(this);
                break;
            case vk:
                connection = new VkConnection(this);
                break;
            case wechat:
                connection = new WeChatConnection(this);
                break;
            case fb:
                connection = new FBConnection(this);
                break;
            case telegram2:
                connection = new TelegramConnection(this);
                break;
            case viberpublic:
                connection = new ViberPublicConnection(this);
                break;
            case telegram3:
                connection = new TelegramBotConnection(this);
                break;
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                connection.init();

            }
        }, 2000);
    }

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBareJid() {
        return new JID(jid).toBareJID();
    }

    public void setResource(String resource) {
        this.resource = "/" + (resource != null ? resource : "mychat");
    }

    public String getFullJid() {
        return jid+resource;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public ApiConnection getConnection() {
        return connection;
    }

    public Server.CHANNEL getChannel() {
        return channel;
    }

    public void setChannel(Server.CHANNEL channel) {
        this.channel = channel;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public void updateChannelName(String channelName) {
        if (!this.channelName.equals(channelName)) {
            this.channelName = channelName;
            DatabaseManager.getInstance().updateChannelName(this);
        }
    }

    public void updateServiceUrl(String serviceUrl) {
        if (!this.serviceUrl.equals(serviceUrl)) {
            this.serviceUrl = serviceUrl;
            DatabaseManager.getInstance().updateServiceURL(this);
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLogin() {
        switch (getChannel()) {
            case facebook:
            case skype:
            case slack:
            case kik:
            case wechat:
                return getAppId();
            case okru:
            case vk:
            case fb:
            case viberpublic:
            case telegram3:
                return getAccessToken();
            case telegram2:
                return getPhone();

        }

        return null;
    }
}
