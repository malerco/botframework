package ru.tro.botframework4xmpp.beans;

/**
 * Created by aleksey on 05.03.17.
 */
public class TokenBean {
    private String token_type;
    private String access_token;
    private int expires_in;

    public String getToken_type() {
        return token_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public long getExpiresIn() {
        return expires_in*1000;
    }
}
