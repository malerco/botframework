package ru.tro.botframework4xmpp.beans;

import ru.tro.botframework4xmpp.Server;

/**
 * Created by aleksey on 29.01.17.
 */
public class ContactBean {
    private String jid;
    private String ownerJid;
    private int ownerId;
    private String id;
    private String name;
    private String conversationId;
    private Server.CHANNEL channel;
    private boolean isOnline = true;



    public ContactBean(String jid, String ownerJid, int ownerId, String id, String name, String conversationId, Server.CHANNEL channel) {
        this.jid = jid;
        this.ownerJid = ownerJid;
        this.ownerId = ownerId;
        this.id = id;
        this.name = name;
        this.conversationId = conversationId;
        this.channel = channel;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getName() {
        return name;
    }

    public String getOwnerJid() {
        return ownerJid;
    }

    public String getJid() {
        return jid;
    }

    public String getConversationId() {
        return conversationId;
    }

    public String getChatId() {
        return getConversationId();
    }

    public Server.CHANNEL getChannel() {
        return channel;
    }

    public void setChannel(Server.CHANNEL channel) {
        this.channel = channel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }
}
