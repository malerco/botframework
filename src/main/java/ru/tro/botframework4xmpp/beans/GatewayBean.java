package ru.tro.botframework4xmpp.beans;

/**
 * Created by aleksey on 29.01.17.
 */
public class GatewayBean {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
