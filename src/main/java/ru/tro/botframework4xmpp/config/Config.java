package ru.tro.botframework4xmpp.config;

/**
 * Created by aleksey on 29.01.17.
 */
public class Config {
    public static final String EOL = System.lineSeparator();

    public static final String CONFIGURATION_FILE = "./config/Server.properties";


    public static boolean DEBUG;
    public static boolean XML_LOG;
    public static boolean JSON_LOG;

    public static String XMPP_HOST;
    public static int XMPP_PORT;
    public static String XMPP_SALT;

    public static String MYSQL_URL;
    public static String MYSQL_USER;
    public static String MYSQL_PASSWORD;

    public static String USER_OFFLINE_MSG = "Error";

    public static int SOCKET_SERVER_PORT;
    public static int API_SOCKET_SERVER_PORT;

    public static String OKRU_WEBHOOK_ADDRESS;
    public static String VIBERPUBLIC_WEBHOOK_ADDRESS;

    public static String[] ADMINS_TO_REPORT;

    public static void load () {
        final PropertiesParser serverSettings = new PropertiesParser(CONFIGURATION_FILE);

        DEBUG = serverSettings.getBoolean("DEBUG", true);
        XML_LOG = serverSettings.getBoolean("XML_LOG", false);
        JSON_LOG = serverSettings.getBoolean("JSON_LOG", false);

        XMPP_HOST = serverSettings.getString("XMPP_HOST", "openfire.mysender.ru");
        XMPP_PORT = serverSettings.getInt("XMPP_PORT", 6275);
        XMPP_SALT = serverSettings.getString("XMPP_SALT", "secret");

        MYSQL_URL = serverSettings.getString("MYSQL_URL", "jdbc:mysql://localhost:3306/botframework?useSSL=false&useUnicode=true&characterEncoding=UTF-8");
        MYSQL_USER = serverSettings.getString("MYSQL_USER", "root");
        MYSQL_PASSWORD = serverSettings.getString("MYSQL_PASSWORD", "vq035q66");


        SOCKET_SERVER_PORT = serverSettings.getInt("SOCKET_SERVER_PORT", 1115);
        API_SOCKET_SERVER_PORT = serverSettings.getInt("API_SOCKET_SERVER_PORT", 1115);
        OKRU_WEBHOOK_ADDRESS = serverSettings.getString("OKRU_WEBHOOK_ADDRESS", "");
        VIBERPUBLIC_WEBHOOK_ADDRESS = serverSettings.getString("VIBERPUBLIC_WEBHOOK_ADDRESS", "");
        ADMINS_TO_REPORT = serverSettings.getString("ADMINS_TO_REPORT", "aleksey@openfire.mysender.ru").split(";");
    }
}
