package ru.tro.botframework4xmpp.listeners;

import com.github.badoualy.telegram.api.TelegramClient;
import com.github.badoualy.telegram.api.UpdateCallback;
import com.github.badoualy.telegram.api.utils.MediaInput;
import com.github.badoualy.telegram.api.utils.TLMediaUtilsKt;
import com.github.badoualy.telegram.tl.api.*;
import com.github.badoualy.telegram.tl.api.messages.TLAbsDialogs;
import com.github.badoualy.telegram.tl.core.TLIntVector;
import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import com.google.gson.JsonParser;
import com.squareup.okhttp.*;
import org.jetbrains.annotations.NotNull;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.handlers.TelegramHandler;
import ru.tro.botframework4xmpp.network.TelegramConnection;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class TelegramListener implements UpdateCallback {
    private UserBean owner;

    public TelegramListener(UserBean owner) {
        this.owner = owner;
    }

    @Override
    public void onUpdates(@NotNull TelegramClient client, @NotNull TLUpdates updates) {
        for (TLAbsUpdate update : updates.getUpdates()) {
            if (!(update instanceof TLUpdateNewMessage)) {
                continue;
            }

            TLAbsMessage absMessage = ((TLUpdateNewMessage) update).getMessage();

            if (!(absMessage instanceof TLMessage)) {
                continue;
            }

            TLMessage message = (TLMessage) absMessage;

            if (message.getOut()) {
                continue;
            }

            ContactBean contactBean = getContact(client, message.getFromId());

            XMPPMessage.sendMessageFromTelegram(owner, contactBean.getJid(), message.getMessage());
            if (message.getMedia() != null) {
                TLAbsMessageMedia media = message.getMedia();

                // Magic utils method from api module
                MediaInput mediaInput = TLMediaUtilsKt.getAbsMediaInput(media);

                if (mediaInput != null) {
                    String filename;
                    if (media instanceof TLMessageMediaPhoto || media instanceof TLMessageMediaWebPage) {
                        filename = "photo-" + System.currentTimeMillis() + ".jpg";
                    } else {
                        // Retrieve real name
                        TLDocument tlDocument = ((TLMessageMediaDocument) media).getDocument().getAsDocument();
                        filename = ((TLDocumentAttributeFilename) tlDocument.getAttributes().stream()
                                .filter(attr -> attr instanceof TLDocumentAttributeFilename)
                                .findFirst().get()).getFileName();
                    }

                    try {
                        File file = new File(TelegramHandler.MEDIA_DIR, filename);
                        FileOutputStream fos = new FileOutputStream(file);

                        client.downloadSync(mediaInput.getInputFileLocation(), mediaInput.getSize(), fos);

                        Path path = Paths.get(file.getAbsolutePath());
                        byte[] data = Files.readAllBytes(path);
                        RequestBody requestBody = new MultipartBuilder()
                                .type(MultipartBuilder.FORM)
                                .addFormDataPart("file", filename,
                                        RequestBody.create(MediaType.parse("image/png"), data))
                                .build();

                        Request request = new Request.Builder()
                                .url("http://main3.mysender.ru/fileupload.php")
                                .post(requestBody)
                                .build();

                        Response response = new OkHttpClient().newCall(request).execute();
                        String answer = response.body().string();
                        XMPPMessage.sendMessageFromTelegram(owner, contactBean.getJid(),
                                new JsonParser().parse(answer).getAsJsonObject().get("url").getAsString());
                        file.delete();
                    } catch (IOException | RpcErrorException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    System.err.println("MessageMedia type not supported" + media.getClass().getSimpleName());
                }
            }
        }
    }

    @Override
    public void onUpdatesCombined(@NotNull TelegramClient client, @NotNull TLUpdatesCombined updates) {

    }

    @Override
    public void onUpdateShort(@NotNull TelegramClient client, @NotNull TLUpdateShort update) {

    }

    @Override
    public void onShortChatMessage(@NotNull TelegramClient client, @NotNull TLUpdateShortChatMessage message) {

    }

    @Override
    public void onShortMessage(@NotNull TelegramClient client, @NotNull TLUpdateShortMessage message) {
        if (message.getOut()) {
            return;
        }

        getContact(client, message.getUserId());

        XMPPMessage.sendMessageFromTelegram(owner, getContact(client, message.getUserId()).getJid(), message.getMessage());

    }

    private ContactBean getContact(TelegramClient client, int userId) {
            ContactBean contactBean = Contacts.getInstance().findById(owner, userId);

        if (contactBean == null) {
            contactBean = new ContactBean(userId + Server.CHANNEL.telegram2.getWithAt(),
                    owner.getJid(),
                    owner.getId(), userId+"",
                    getNickFromUserId(client, userId),
                    userId+"",
                    Server.CHANNEL.telegram2);
            Contacts.getInstance().tryInsert(Server.CHANNEL.telegram2, contactBean);
        }

        try {
            ((TelegramConnection) owner.getConnection()).setList(client.messagesGetDialogs(false, 0, 0, new TLInputPeerUser(), 10).getUsers());
        } catch (RpcErrorException | IOException e) {
            e.printStackTrace();
        }

        return contactBean;
    }

    @Override
    public void onShortSentMessage(@NotNull TelegramClient client, @NotNull TLUpdateShortSentMessage message) {

    }

    @Override
    public void onUpdateTooLong(@NotNull TelegramClient client) {

    }

    private String getNickFromUserId(TelegramClient client, int userId) {
        String nick = "";
        try {
            String first = null;
            TLUser tlUser = client.usersGetFullUser(new TLInputUser(userId, 0)).getUser().getAsUser();
            first = tlUser.getFirstName();
            String last = tlUser.getLastName();
            if (first != null && !first.equals("null") && !first.isEmpty()) {
                nick += first;
            }

            if (last != null && !last.equals("null") && !last.isEmpty()) {
                if (nick.isEmpty())
                    nick += last;
                else
                    nick += " " + last;
            }
        } catch (RpcErrorException | IOException e) {
            TLAbsDialogs tlAbsDialogs = null;
            try {
                tlAbsDialogs = client.messagesGetDialogs(false, 0, 0, new TLInputPeerUser(), 100);
                // Map peer id to displayable string
                HashMap<Integer, String> nameMap = createNameMap(tlAbsDialogs);
                nick += nameMap.get(getId(tlAbsDialogs.getDialogs().get(0).getPeer())).replace("null", "").trim();
            } catch (RpcErrorException | IOException e1) {
                nick += userId;
                e1.printStackTrace();
            }
        }

        return nick;
    }


    /**
     * @param tlAbsDialogs result from messagesGetDialogs
     * @return a map where the key is the peerId and the value is the chat/channel title or the user's name
     */
    static HashMap<Integer, String> createNameMap(TLAbsDialogs tlAbsDialogs) {
        // Map peer id to name
        HashMap<Integer, String> nameMap = new HashMap<>();

        tlAbsDialogs.getUsers().stream()
                .map(TLAbsUser::getAsUser)
                .forEach(user -> nameMap.put(user.getId(),
                        user.getFirstName() + " " + user.getLastName()));

        tlAbsDialogs.getChats().forEach(chat -> {
                    if (chat instanceof TLChannel) {
                        nameMap.put(chat.getId(), ((TLChannel) chat).getTitle());
                    } else if (chat instanceof TLChannelForbidden) {
                        nameMap.put(chat.getId(), ((TLChannelForbidden) chat).getTitle());
                    } else if (chat instanceof TLChat) {
                        nameMap.put(chat.getId(), ((TLChat) chat).getTitle());
                    } else if (chat instanceof TLChatEmpty) {
                        nameMap.put(chat.getId(), "Empty chat");
                    } else if (chat instanceof TLChatForbidden) {
                        nameMap.put(chat.getId(), ((TLChatForbidden) chat).getTitle());
                    }
                });

        return nameMap;
    }

    public static int getId(TLAbsPeer peer) {
        if (peer instanceof TLPeerUser) {
            return ((TLPeerUser) peer).getUserId();
        }
        if (peer instanceof TLPeerChat) {
            return ((TLPeerChat) peer).getChatId();
        }

        return ((TLPeerChannel) peer).getChannelId();
    }
}
