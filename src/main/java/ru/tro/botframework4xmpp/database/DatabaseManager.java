package ru.tro.botframework4xmpp.database;

import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

public class DatabaseManager 
{	
	private static final Logger _log = Logger.getLogger(DatabaseManager.class.getName());
	
	protected DatabaseManager()
	{
		reloadAll();
	}
	
	/**
	 * Загружает всех пользователей из базы создавая каждому отдельный инстанс
	 */
	private void reloadAll() 
	{
		HashMap<Integer, ArrayList<String>> mysql;
		
		String query = "SELECT * FROM users";
		mysql = MySQLDriver.getRows(query);
		
		if (!mysql.isEmpty()) {
			for(ArrayList<String> data : mysql.values())
			{
				UserBean user = new UserBean(Server.CHANNEL.valueOf(data.get(2)));
				user.setId(Integer.parseInt(data.get(0)));
				user.setJid(data.get(1));
				user.setChannelId(data.get(3));
				user.setChannelName(data.get(4));
				user.setServiceUrl(data.get(5));
				user.setAppId(data.get(6));
				user.setAppSecret(data.get(7));
				user.setPhone(data.get(8));
				user.setAccessToken(data.get(9));
				Users.getInstance().add(user);
			}
		}
		_log.info("Loaded "+mysql.size()+" users from database.");
		
		query = "SELECT * FROM contacts";
		mysql = MySQLDriver.getRows(query);

		if (!mysql.isEmpty()) {
			for (ArrayList<String> data : mysql.values()) {
				ContactBean contact =
						new ContactBean(
								data.get(0),
								data.get(2),
								Integer.parseInt(data.get(1)),
								data.get(3),
								data.get(4),
								data.get(5),
								Server.CHANNEL.valueOf(data.get(6)));
				Contacts.getInstance().add(contact);
			}
		}
		_log.info("Loaded "+mysql.size()+" contacts from database.");
	}
	

	/**
	 * Добавляет в базу новый контакт указанному пользователю
	 * @param contact
	 */
	public static void addContact(ContactBean contact)
	{
		String query = "INSERT INTO contacts (jid,owner_id,owner_jid,id,name,conversation_id,channel) VALUES('" + contact.getJid() + "', " +
				"'" + contact.getOwnerId() + "', '" + contact.getOwnerJid() + "', '" + contact.getId() + "', '" + contact.getName() + "', " +
				"'" + contact.getConversationId() + "', '" + contact.getChannel() + "')";
		if (Config.DEBUG)
			System.out.println(query);
		MySQLDriver.update(query);
	}

	/**
	 * Добавляет нового пользователя в базу
	 */
	public static void addNewUser(UserBean user)
	{
		String query = "INSERT INTO `users`(`jid`,`channel`,`channel_id`,`channel_name`,`service_url`,`app_id`,`app_secret`,`phone`,`token`, creation_date) VALUES " +
				"('"+user.getJid()+"','"+user.getChannel() +"','"+user.getChannelId() +"','"+user.getChannelName() +"'," +
				"'"+user.getServiceUrl() +"'," + "'"+user.getAppId() +"','"+user.getAppSecret() +"', '" + user.getPhone() + "','"+user.getAccessToken() +"', NOW()) ";
		MySQLDriver.update(query);
		query = "SELECT id FROM users ORDER BY id DESC LIMIT 1";
		int userId = Integer.parseInt(MySQLDriver.getRow(query).get(0));

		user.setId(userId);
		Users.getInstance().add(user);
	}

	public static DatabaseManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public void updateServiceURL(UserBean userBean) {
		String query = "UPDATE users SET service_url = '" + userBean.getServiceUrl() + "' " +
				"WHERE jid = '" + userBean.getJid() + "' AND channel = '" + userBean.getChannel().name() + "'";
		MySQLDriver.update(query);
	}

	public void updateChannelName(UserBean userBean) {
		String query = "UPDATE users SET channel_name = '" + userBean.getChannelName() + "' " +
				"WHERE jid = '" + userBean.getJid() + "' AND channel = '" + userBean.getChannel().name() + "'";
		MySQLDriver.update(query);

	}

	public static void removeUserById(UserBean user) {
		String query = "DELETE FROM `users` WHERE id = " + user.getId();
		MySQLDriver.update(query);
		user.getConnection().closeSession();
		Users.getInstance().remove(user);
		query = "DELETE FROM contacts WHERE owner_id = " + user.getId();
		Contacts.getInstance().removeAll(Contacts.getInstance().findAllByOwnerId(user));
		MySQLDriver.update(query);
	}

	private static class SingletonHolder
	{
		protected static final DatabaseManager _instance = new DatabaseManager();
	}

}
