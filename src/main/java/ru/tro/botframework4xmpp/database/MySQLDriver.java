package ru.tro.botframework4xmpp.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import ru.tro.botframework4xmpp.config.Config;

public class MySQLDriver {
	private static final Logger _log = Logger.getLogger(MySQLDriver.class.getName());
	
	// JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

	private final static String initUsersQuery =  "CREATE TABLE `users` (\n" +
			"\t`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
			"\t`jid` VARCHAR(64) NOT NULL,\n" +
			"\t`channel` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`channel_id` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`channel_name` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`service_url` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`app_id` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`app_secret` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`token` VARCHAR(256) NOT NULL DEFAULT '-1',\n" +
			"\t`phone` VARCHAR(25) NOT NULL DEFAULT '-1',\n" +
			"\t`creation_date` DATE NOT NULL\n" +
			") ENGINE=MyISAM DEFAULT CHARSET=utf8;\n";
	private final static String initContactsQuery = "CREATE TABLE `contacts` (\n" +
			"\t`jid` VARCHAR(128) NOT NULL,\n" +
			"\t`owner_id` INT NOT NULL,\n" +
			"\t`owner_jid` VARCHAR(64) NOT NULL,\n" +
			"\t`id` VARCHAR(64) NOT NULL,\n" +
			"\t`name` VARCHAR(64) NOT NULL,\n" +
			"\t`conversation_id` VARCHAR(256) NOT NULL,\n" +
			"\t`channel` VARCHAR(256) NOT NULL\n" +
			") ENGINE=MyISAM DEFAULT CHARSET=utf8;\n";

	/**
     * Функция получает возвращает массив строк из базы по запросу
     * @param query
     * @return
     */
	public static ArrayList<String> getRow(String query) 
	{
		ArrayList<String> result = new ArrayList<>();


		try
		{
            // opening database connection to MySQL server
            con = DriverManager.getConnection(Config.MYSQL_URL, Config.MYSQL_USER, Config.MYSQL_PASSWORD);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            rs = stmt.executeQuery(query);

            while (rs.next()) {
            	int col = rs.getMetaData().getColumnCount();
            	for (int i=0; i<col; i++)
            	{
            		result.add(rs.getString(i+1));
            	}
            }
            rs.close();
            stmt.close();
            con.close();
        }
		catch (Exception sqlEx)
		{
            _log.info("Error in MySQLDriver.getRow(). Error: " + sqlEx.getMessage());
        }

		return result;
	}
	
	/**
	 * Функция возвращает HashMap массивов строк из базы
	 * в случае когда вывод из базы не 1 строка
	 */
	public static HashMap<Integer, ArrayList<String>> getRows(String query)
	{
		HashMap<Integer, ArrayList<String>> result = new HashMap<>();

		try {
			Class.forName("com.mysql.jdbc.Driver");
            // opening database connection to MySQL server
            con = DriverManager.getConnection(Config.MYSQL_URL, Config.MYSQL_USER, Config.MYSQL_PASSWORD);

            // getting Statement object to execute query
            stmt = con.createStatement();
		} catch (Exception sqlEx) {
			_log.info("Error in MySQLDriver.getRows(). Error: " + sqlEx.toString());
		}

			// executing SELECT query
		boolean newDatabase = false;
		try {
			rs = stmt.executeQuery(query);
			int counter = 0;
			while (rs.next()) {
				ArrayList<String> list = new ArrayList<>();
				int col = rs.getMetaData().getColumnCount();
				for (int i=0; i<col; i++)
				{
					list.add(rs.getString(i+1));
				}
				result.put(counter, list);
				counter++;
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException e) {
			_log.info("Error in MySQLDriver.getRows(). Error: " + e.toString());
			_log.info("Initializing tables!");
			newDatabase = true;
		}
		if (newDatabase) {
			try {
				stmt.executeUpdate(initUsersQuery);
			} catch (SQLException e1) {
				e1.printStackTrace();
				_log.info("Error while initializing table USERS! " + e1.getMessage());
			}
			try {
				stmt.executeUpdate(initContactsQuery);
			} catch (SQLException e1) {
				e1.printStackTrace();
				_log.info("Error while initializing table CONTACTS! " + e1.getMessage());
			}
		}
		
		return result;
	}

	/**
	 * Фукция выполняющая запрос в базу без возврата значения
	 * @param query
	 */
	public static void update(String query) {
		try
		{
            con = DriverManager.getConnection(Config.MYSQL_URL, Config.MYSQL_USER, Config.MYSQL_PASSWORD);
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
            con.close();
        }
		catch (Exception sqlEx)
		{
			sqlEx.printStackTrace();
			_log.info("Error in MySQLDriver.tryInsert(). Error: " + sqlEx.getMessage());
        }
	}
}
