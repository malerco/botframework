package ru.tro.botframework4xmpp.singleton;

import org.xmpp.packet.Packet;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.handlers.PresenceHandler;
import ru.tro.botframework4xmpp.handlers.RosterHandler;

import java.util.ArrayList;

/**
 * Created by aleksey on 29.01.17.
 */
public class Contacts extends ArrayList<ContactBean> {
    private static Contacts ourInstance = new Contacts();

    public static Contacts getInstance() {
        return ourInstance;
    }

    private Contacts() {
    }

    public ArrayList<ContactBean> findAllByOwner(Server.CHANNEL channel, String ownerJid) {
        ArrayList<ContactBean> list = new ArrayList<>();
        for (ContactBean bean : this) {
            if (bean.getOwnerJid().equalsIgnoreCase(ownerJid) &&
                    bean.getChannel() == channel) {
                list.add(bean);
            }
        }

        return list;
    }

    public ContactBean findByJid(String jid) {
        for (ContactBean bean : this) {
            if (bean.getJid().equalsIgnoreCase(jid)) {
                return bean;
            }
        }

        return null;
    }

    @Override
    public boolean add(ContactBean contactBean) {
        if (!this.contains(contactBean)) {
            super.add(contactBean);
            return true;
        }
        return false;
    }

    public void tryInsert(Server.CHANNEL channel, ContactBean contactBean) {
        if (add(contactBean)) {
            DatabaseManager.addContact(contactBean);

            //send contact to xmpp botframeworkclient
            UserBean user = Users.getInstance().findUserByJid(channel, contactBean.getOwnerJid());
            ArrayList<Packet> packetList = new ArrayList<>();

            packetList.add(RosterHandler.genRoster(user, contactBean.getJid(), contactBean.getName(), user.getChannel().name(), "to"));
            packetList.add(PresenceHandler.genStatus(user, contactBean.getJid(), null));
            ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(packetList);
        }
    }

    @Override
    public boolean contains(Object o) {
        ContactBean contactBean = (ContactBean) o;
        for (ContactBean bean : this) {
            if (contactBean.getJid().equals(bean.getJid()) &&
                    contactBean.getOwnerJid().equals(bean.getOwnerJid())) {
                return true;
            }
        }
        return false;
    }

    public ContactBean find(String jid, int ownerId) {
        for (ContactBean bean : this) {
            if (bean.getJid().equals(jid) && bean.getOwnerId() == ownerId) {
                return bean;
            }
        }
        return null;
    }

    public ContactBean findById(UserBean owner, int contactId) {
        for (ContactBean contactBean : this) {
            if (contactBean.getId() != null &&
                    contactBean.getOwnerJid().equals(owner.getJid()) &&
                    contactBean.getId().equals(contactId + ""))
                return contactBean;
        }

        return null;
    }

    public ArrayList<ContactBean> findAllByOwnerId(UserBean user) {
        ArrayList<ContactBean> toReturn = new ArrayList<>();
        for (ContactBean contact : this) {
            if (contact.getOwnerId() == user.getId()) {
                toReturn.add(contact);
            }
        }

        return toReturn;
    }
}
