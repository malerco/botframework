package ru.tro.botframework4xmpp.singleton;

import clientapi.vkclient.model.WebHookObject;
import com.google.gson.Gson;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.threads.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by aleksey on 25.02.17.
 */
public class WebhookSocketServer {
    private static final Logger _log = Logger.getLogger(WebhookSocketServer.class.getName());
    private static WebhookSocketServer ourInstance = new WebhookSocketServer();

    public static WebhookSocketServer getInstance() {
        return ourInstance;
    }

    private SocketThread thread;
    private WebhookSocketServer() {
        thread = new SocketThread();
        thread.start();
    }

    public void stopSocketServer() {
        thread.interrupt();
        thread = null;
    }

    private class SocketThread extends Thread {
        private ServerSocket server;
        private BufferedReader in;
        private boolean stopSignal = false;

        @Override
        public void run() {
            while (!stopSignal) {
                try {
                    server = new ServerSocket(Config.SOCKET_SERVER_PORT);

                    Socket socket = server.accept();

                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    _log.info("STOP SIGNAL: " + stopSignal);
                    while (!stopSignal) {
                        String data = in.readLine();
                        if (data == null) {
                            _log.info("DATA NULL");
                            break;
                        }
                        else {
                            _log.info("WebhookSocket DATA: " + data);
                            Server.CHANNEL CHNL;
                            try {
                                CHNL = Server.CHANNEL.valueOf(
                                        new Gson().fromJson(data, WebHookObject.class).getChannel());
                            } catch (Exception exc) {
                                exc.printStackTrace();
                                continue;
                            }

                            switch (CHNL) {
                                case skype:
                                case facebook:
                                case slack:
                                case kik:
                                    new BotFMessageParser(data).start();
                                    break;
                                case vk:
                                    new VkMessageParser(data).start();
                                    break;
                                case okru:
                                    new OkMessageParser(data).start();
                                    break;
                                case wechat:
                                    new WeChatMessageParser(data).start();
                                    break;
                                case fb:
                                    new FBMessageParser(data).start();
                                    break;
                                case viberpublic:
                                    new ViberPublicMessageParser(data).start();
                                    break;

                            }
                        }
                    }
                    socket.close();
                } catch (Exception e){
                    e.printStackTrace();
                    try {
                        Thread.sleep(50000);
                    } catch (InterruptedException e1) {

                    }
                } finally {
                    try {
                        _log.info("SOCKET CLOSE");
                        server.close();
                    } catch (NullPointerException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
 }

