package ru.tro.botframework4xmpp.singleton;

import com.google.gson.Gson;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.handlers.RegisterHandler;
import ru.tro.botframework4xmpp.network.TelegramConnection;
import ru.tro.botframework4xmpp.transportlayer.XmppRegPacket;
import ru.tro.botframework4xmpp.transportlayer.XmppResponsePacket;
import ru.tro.botframework4xmpp.utils.Crypt;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import static ru.tro.botframework4xmpp.Server.CHANNEL.telegram2;

/**
 * Created by aleksey on 25.02.17.
 */
public class ApiSocketServer {
    private static final Logger _log = Logger.getLogger(ApiSocketServer.class.getName());
    private static ApiSocketServer ourInstance = new ApiSocketServer();

    public static ApiSocketServer getInstance() {
        return ourInstance;
    }

    private SocketThread thread;
    private ApiSocketServer() {
        thread = new SocketThread();
        thread.start();
    }


    public void stopSocketServer() {
        thread.interrupt();
        thread = null;
    }

    private class SocketThread extends Thread {
        private ServerSocket server;
        private BufferedReader in;
        private PrintWriter out;

        @Override
        public void run() {
            try {
                server = new ServerSocket(Config.API_SOCKET_SERVER_PORT);

            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            while (true) {
                Socket socket = null;
                boolean success;
                try {
                    socket = server.accept();
                    socket.setSoTimeout(10000);
                    out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String data = in.readLine();
                    if (data == null)
                        return;

                    String decryptedData = new Crypt().decrypt(data);
                    XmppRegPacket regPacket = new Gson().fromJson(decryptedData, XmppRegPacket.class);
                    XmppResponsePacket responsePacket = new XmppResponsePacket();
                    _log.info("XMPP RESPONSE PACKAGE: " + responsePacket.toString());
                    switch (regPacket.getAction()) {
                        case list:
                            responsePacket.setList(Users.getInstance().getLoginValues(regPacket.getChannel(), regPacket.getJid()));
                            responsePacket.setSuccess(true);
                            break;
                        case reg:
                            if (regPacket.getPhone() != null &&
                                    !regPacket.getPhone().isEmpty() && regPacket.getChannel() == telegram2 && !regPacket.getPhone().startsWith("+")) {
                                regPacket.setPhone("+".concat(regPacket.getPhone()));
                            }

                            UserBean user = Users.getInstance().findUserByLogin(regPacket.getChannel(), regPacket.getAppId(), regPacket.getAccessToken(), regPacket.getPhone());
                            if (user != null && regPacket.getChannel() == Server.CHANNEL.telegram2 && regPacket.getSmsCode() != null) {
                                success = ((TelegramConnection)user.getConnection()).sendCode(regPacket.getSmsCode());

                                if (success) {
                                    responsePacket.setSuccess(true);
                                } else {
                                    responsePacket.setError("Registration failed");
                                }

                            } else if (user != null) {
                                responsePacket.setError("This login already exists");

                            } else {
                                success = RegisterHandler.registerUser(
                                        regPacket.getChannel(),
                                        regPacket.getJid(),
                                        regPacket.getAppId(),
                                        regPacket.getAppSecret(),
                                        regPacket.getAccessToken(),
                                        regPacket.getPhone(),
                                        null);

                                if (success) {
                                    responsePacket.setSuccess(true);
                                } else {
                                    responsePacket.setError("Registration failed");
                                }
                            }
                            break;
                        case unreg:
                            success = RegisterHandler.unregisterUser(regPacket.getChannel(), regPacket.getUnregLogin(), null);

                            if (success) {
                                responsePacket.setSuccess(true);
                            } else {
                                responsePacket.setError("Registration failed");
                            }
                            break;
                        case reloadcfg:
                            Config.load();
                            responsePacket.setSuccess(true);
                            break;
                    }

                    out.println(new Crypt().encrypt(new Gson().toJson(responsePacket)));
                    socket.close();

                } catch(Exception e){
                    e.printStackTrace();
                } finally{
                    try {
                        if (socket != null) {
                            _log.info("API SOCKET CLOSED");
                            socket.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
 }

