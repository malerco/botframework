package ru.tro.botframework4xmpp.singleton;

import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.UserBean;

import java.util.ArrayList;

/**
 * Created by aleksey on 29.01.17.
 */
public class Users  extends ArrayList<UserBean> {
    private static Users ourInstance = new Users();

    public static Users getInstance() {
        return ourInstance;
    }

    private Users() {
    }

    public UserBean findUserByJid(Server.CHANNEL channel, String bareJid) {
        for (UserBean bean : this) {
            if (bean.getBareJid().equalsIgnoreCase(bareJid) &&
                    bean.getChannel() == channel) {
                return bean;
            }
        }
        return null;
    }

    public boolean containsJid(Server.CHANNEL channel, String s) {
        for (UserBean bean : this) {
            if (bean.getBareJid().equalsIgnoreCase(s) &&
                    bean.getChannel() == channel) {
                return true;
            }
        }
        return false;
    }

    public boolean containsCredentials(Server.CHANNEL chnl, String appId, String token) {
        for (UserBean bean : this) {
            switch (chnl) {
                case facebook:
                case skype:
                case slack:
                case kik:
                    if (bean.getAppId().equalsIgnoreCase(appId) &&
                            bean.getChannel() == chnl) {
                        return true;
                    }
                    break;
                case vk:
                case okru:
                    if (bean.getAccessToken() != null &&
                            bean.getAccessToken().equalsIgnoreCase(token) &&
                            bean.getChannel() == chnl) {
                        return true;
                    }
            }
        }
        return false;
    }

    public UserBean findUserByChannelId(String id) {
        for (UserBean bean : this) {
            if (id.equals(bean.getChannelId())) {
                return bean;
            }
        }
        return null;
    }

    public ArrayList<String> getLoginValues(Server.CHANNEL channel, String jid) {
        ArrayList<String> toReturn = new ArrayList<>();
        for (UserBean user : this) {

            if (user.getJid().equals(jid) && user.getChannel().equals(channel)) {
                toReturn.add(user.getLogin());
            }
        }
        return toReturn;
    }

    public UserBean findUserByLogin(Server.CHANNEL channel, String... choosedLogin) {
        for (String login : choosedLogin) {
            if (login != null && !login.equals("-1")) {
                for (UserBean userBean : this) {
                    if (userBean.getChannel() == channel && userBean.getLogin().equals(login))
                        return userBean;
                }
            }
        }
        return null;
    }

    public UserBean findUserById(int ownerId) {
        for (UserBean user : this) {
            if (user.getId() == ownerId) {
                return user;
            }
        }

        return null;
    }
}
