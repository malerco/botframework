package ru.tro.botframework4xmpp.singleton;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;

public class TelegramBot {
    private static TelegramBot ourInstance = new TelegramBot();

    public static TelegramBot getInstance() {
        return ourInstance;
    }

    private TelegramBot() {
    }

    private final String BROADCAST_ID = "-221058199";
    private final String BOT_KEY = "bot401216066:AAE2L_bknSSwXUnLohfI-EXWwMSqF1o2u3o";
    private final String TG_URL = "api.telegram.org";
    private final OkHttpClient httpClient = new OkHttpClient();


    public void sendBroadcast (String message) {

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host(TG_URL)
                .addPathSegment(BOT_KEY)
                .addPathSegment("sendMessage")
                .addQueryParameter("chat_id", BROADCAST_ID)
                .addQueryParameter("text", message)
                .build();
        Request request = new Request.Builder().url(url).build();

        try {
            String answer = httpClient.newCall(request).execute().body().string();
        } catch (IOException ignored) {

        }
    }
}
