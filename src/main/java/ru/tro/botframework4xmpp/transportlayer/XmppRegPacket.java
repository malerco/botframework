package ru.tro.botframework4xmpp.transportlayer;

import ru.tro.botframework4xmpp.Server.CHANNEL;
import ru.tro.botframework4xmpp.config.Config;

public class XmppRegPacket {

    public String getUnregLogin() {
        return unregLogin;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public enum ACTION {reg, unreg, list, reloadcfg}

    private ACTION action;
    private CHANNEL channel;
    private String account;
    private String appId;
    private String appSecret;
    private String accessToken;
    private String phone;
    private String unregLogin;
    private String smsCode;

    public XmppRegPacket(ACTION action) {
        this.action = action;
    }

    public ACTION getAction() {
        return action;
    }

    public void setUnregLogin(String unregLogin) {
        this.unregLogin = unregLogin;
    }

    public CHANNEL getChannel() {
        return channel;
    }

    public void setChannel(CHANNEL channel) {
        this.channel = channel;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJid() {
        return account + "@" + Config.XMPP_HOST;
    }
}
