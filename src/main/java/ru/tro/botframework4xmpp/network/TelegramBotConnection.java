package ru.tro.botframework4xmpp.network;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import clientapi.okruclient.OkRuClient;
import clientapi.okruclient.model.message.OkRuMessageBean;
import clientapi.telegrambotclient.TelegramBotClient;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;


public class TelegramBotConnection implements ApiConnection {

    private static TelegramBotsApi botApi = new TelegramBotsApi();
    private TelegramBotClient client;
    private SendMessage sendMessage;
    private UserBean owner;


    public TelegramBotConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init(){
            try {
                client = new TelegramBotClient(owner);
                botApi.registerBot(client);
                Server.printSection("Bot registered");
            } catch (TelegramApiException e) {
                Server.printSection(e.getMessage());
            }
    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        if (sendMessage == null) sendMessage = new SendMessage();
        sendMessage
                .setChatId(contact.getId())
                .setText(message);
        try {
            client.sendMesaageToUser(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeSession() {
        client = null;
    }
}