package ru.tro.botframework4xmpp.network;

import com.google.gson.JsonParser;
import com.squareup.okhttp.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by aleksey on 07.05.17.
 */
public class UploadHandler {

    public String upload(String name, String contentType, ByteArrayOutputStream buffer) throws IOException {
        String filename = name.length() > 10 ?
                name.substring(name.length()-10) :
                name;

        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("file", filename,
                        RequestBody.create(MediaType.parse(contentType), buffer.toByteArray()))
                .build();

        Request request = new Request.Builder()
                .url("http://main3.mysender.ru/fileupload.php")
                .post(requestBody)
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        String answer = response.body().string();
        return new JsonParser().parse(answer).getAsJsonObject().get("url").getAsString();

    }
}
