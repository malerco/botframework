package ru.tro.botframework4xmpp.network;

import ru.tro.botframework4xmpp.beans.ContactBean;

/**
 * Created by aleksey on 07.05.17.
 */
public interface ApiConnection {
    void init();
    void sendMessage(ContactBean contact, String message);

    void closeSession();
}
