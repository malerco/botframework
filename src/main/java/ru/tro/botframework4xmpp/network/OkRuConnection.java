package ru.tro.botframework4xmpp.network;

import clientapi.okruclient.OkRuClient;
import clientapi.okruclient.model.message.OkRuMessageBean;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.io.IOException;

/**
 * Created by aleksey on 07.05.17.
 */
public class OkRuConnection implements ApiConnection {

    private UserBean owner;

    public OkRuConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {

    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        OkRuMessageBean msg = new OkRuMessageBean(contact.getConversationId(), message);
        try {
            new OkRuClient(owner.getAccessToken()).sendMessage(msg);
        } catch (IOException e) {
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from OkRuConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());

       }
    }

    @Override
    public void closeSession() {

    }
}
