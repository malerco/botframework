package ru.tro.botframework4xmpp.network;

import clientapi.vkclient.VkClient;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.io.IOException;

/**
 * Created by aleksey on 07.05.17.
 */
public class VkConnection implements ApiConnection {

    private UserBean owner;

    public VkConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {

    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        FormEncodingBuilder formBuilder = new FormEncodingBuilder()
                .add("v", "5.64")
                .add("message", message)
                .add("user_id", contact.getId())
                .add("access_token", owner.getAccessToken());
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url("https://api.vk.com/method/messages.send")
                .post(formBody)
                .build();
        try {
            new VkClient(owner.getAccessToken()).sendMessage(request);
        } catch (IOException e) {
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from VkConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());

            e.printStackTrace();
        }
    }

    @Override
    public void closeSession() {

    }
}
