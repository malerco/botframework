package ru.tro.botframework4xmpp.network;

import com.github.badoualy.telegram.api.Kotlogram;
import com.github.badoualy.telegram.api.TelegramApiStorage;
import com.github.badoualy.telegram.api.TelegramClient;
import com.github.badoualy.telegram.mtproto.auth.AuthKey;
import com.github.badoualy.telegram.mtproto.model.DataCenter;
import com.github.badoualy.telegram.mtproto.model.MTSession;
import com.github.badoualy.telegram.tl.api.TLAbsUser;
import com.github.badoualy.telegram.tl.api.TLInputPeerUser;
import com.github.badoualy.telegram.tl.api.TLUser;
import com.github.badoualy.telegram.tl.api.auth.TLAuthorization;
import com.github.badoualy.telegram.tl.api.auth.TLSentCode;
import com.github.badoualy.telegram.tl.core.TLVector;
import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.xmpp.packet.Packet;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.handlers.MessageGenerator;
import ru.tro.botframework4xmpp.handlers.TelegramHandler;
import ru.tro.botframework4xmpp.listeners.TelegramListener;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.TelegramBot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static ru.tro.botframework4xmpp.handlers.TelegramHandler.CLIENTS_DIR;

public class TelegramConnection implements ApiConnection {
    private File AUTH_KEY_FILE;
    private File AUTH_COMPLETE_DIR;
    private File NEAREST_DC_FILE;
    private UserBean owner;
    private TLSentCode sentCode;
    private TelegramClient client;
    private ApiStorage apiStorage;
    private TelegramListener telegramListener;
    private TLVector<TLAbsUser> contacts;

    public TelegramConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {
        AUTH_COMPLETE_DIR = new File(CLIENTS_DIR + File.separator + owner.getPhone() + File.separator, "auth.complete");
        AUTH_KEY_FILE = new File(CLIENTS_DIR + File.separator + owner.getPhone() + File.separator, "auth.key");
        NEAREST_DC_FILE = new File(CLIENTS_DIR + File.separator + owner.getPhone() + File.separator, "dc.save");
        apiStorage = new ApiStorage();
        telegramListener = new TelegramListener(owner);

        if (client == null) {
            client = Kotlogram.getDefaultClient(TelegramHandler.application, apiStorage, telegramListener, Kotlogram.PROD_DC2);
        }

        if (!TelegramHandler.hasTelegramAuth(owner.getPhone())) {
            try {
                sentCode = client.authSendCode(false, owner.getPhone(), true);
                Packet authMessage = MessageGenerator.genMsg(owner.getJid(),
                        Server.CHANNEL.telegram2.getWithDomain(), "enter the phone number and authorization code using the spacebar: \n" +
                                "For example: " + owner.getPhone() + " 66743");
                //ChannelSwitcher.getInstance().getComponent(Server.CHANNEL.telegram2).sendPacket(authMessage);
                TelegramBot.getInstance().sendBroadcast("Client " + owner.getJid() + " with phone " + owner.getPhone() + " need to reauth!!");
            } catch (RpcErrorException | IOException e) {
                Packet authMessage = MessageGenerator.genMsg(owner.getJid(),
                        Server.CHANNEL.telegram2.getWithDomain(), e.getMessage());
                TelegramBot.getInstance().sendBroadcast("Client " + owner.getJid() + " with phone " + owner.getPhone() + " has error!!" +
                        "\n" + e.getMessage());
                //ChannelSwitcher.getInstance().getComponent(Server.CHANNEL.telegram2).sendPacket(authMessage);

                e.printStackTrace();
            }

        } else {
            try {
                setList(client.messagesGetDialogs(false, 0, 0, new TLInputPeerUser(), 100).getUsers());

            } catch (RpcErrorException | IOException e) {
                try {
                    FileUtils.deleteDirectory(AUTH_COMPLETE_DIR);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        init();
                    }
                }, 5000);
            }

        }
    }

    public Long getHash(int userId) {
        for (TLAbsUser user : contacts) {

            if (user.getId() == userId) {
                return user.getAsUser().getAccessHash();
            }
        }
        return 0L;
    }

    public boolean sendCode(String code) {
        if (sentCode == null) {
            return false;
        }
        try {
            TLAuthorization authorization = null;
            try {
                authorization = client.authSignIn(owner.getPhone(), sentCode.getPhoneCodeHash(), code);
            } catch (RpcErrorException ex) {
                if (ex.getMessage().contains("PHONE_NUMBER_UNOCCUPIED")) {
                    authorization = client.authSignUp(owner.getPhone(), sentCode.getPhoneCodeHash(), code, "User", "");
                } else {
                    throw new Exception("auth signup was failed");
                }
            }
            TLUser self = authorization.getUser().getAsUser();
            if (self == null) {
                throw new RpcErrorException(666, "WHAT??");
            }

            Packet authMessage = MessageGenerator.genMsg(owner.getJid(),
                    Server.CHANNEL.telegram2.getWithDomain(), "Auth complete. Hello, " + self.getFirstName());
            //ChannelSwitcher.getInstance().getComponent(Server.CHANNEL.telegram2).sendPacket(authMessage);
            TelegramBot.getInstance().sendBroadcast("Client " + owner.getJid() + " with phone " + owner.getPhone() + " auth success!!");

            sentCode = null;
            AUTH_COMPLETE_DIR.mkdirs();
            return true;
        } catch (Exception e)

        {
            Packet authMessage = MessageGenerator.genMsg(owner.getJid(),
                    Server.CHANNEL.telegram2.getWithDomain(), "Auth failed: " + e.getMessage());
            XMPPMessage.sendWarning(Server.CHANNEL.telegram2, "Auth failed for phone " + owner.getPhone() + " jid: " + owner.getJid());
            ChannelSwitcher.getInstance().getComponent(Server.CHANNEL.telegram2).sendPacket(authMessage);
            e.printStackTrace();
            init();
            return false;
        }
    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        client.messagesSendMessage(new TLInputPeerUser(Integer.parseInt(contact.getId()), getHash(Integer.valueOf(contact.getId()))),
                message,
                Math.abs(new Random().nextLong()));

    }

    public void closeSession() {
        if (client != null) {
            client.close(false);
        }
    }

    public void setList(TLVector<TLAbsUser> list) {
        this.contacts = list;
    }


    public class ApiStorage implements TelegramApiStorage {

        @Override
        public void saveAuthKey(@NotNull AuthKey authKey) {
            try {
                FileUtils.writeByteArrayToFile(AUTH_KEY_FILE, authKey.getKey());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Nullable
        @Override
        public AuthKey loadAuthKey() {
            try {
                return new AuthKey(FileUtils.readFileToByteArray(AUTH_KEY_FILE));
            } catch (IOException e) {
                if (!(e instanceof FileNotFoundException))
                    e.printStackTrace();
            }

            return null;
        }

        @Override
        public void saveDc(@NotNull DataCenter dataCenter) {
            try {
                FileUtils.write(NEAREST_DC_FILE, dataCenter.toString(), Charset.forName("UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Nullable
        @Override
        public DataCenter loadDc() {
            try {
                String[] infos = FileUtils.readFileToString(NEAREST_DC_FILE, Charset.forName("UTF-8")).split(":");
                return new DataCenter(infos[0], Integer.parseInt(infos[1]));
            } catch (IOException e) {
                if (!(e instanceof FileNotFoundException))
                    e.printStackTrace();
            }

            return Kotlogram.PROD_DC4;
        }

        @Override
        public void deleteAuthKey() {
            try {
                FileUtils.forceDelete(AUTH_KEY_FILE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void deleteDc() {
            try {
                FileUtils.forceDelete(NEAREST_DC_FILE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void saveSession(@Nullable MTSession session) {

        }

        @Nullable
        @Override
        public MTSession loadSession() {
            return null;
        }
    }
}
