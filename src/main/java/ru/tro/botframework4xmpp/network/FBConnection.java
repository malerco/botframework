package ru.tro.botframework4xmpp.network;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.types.send.IdMessageRecipient;
import com.restfb.types.send.Message;
import com.restfb.types.send.SendResponse;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;


/**
 * Created by aleksey on 07.05.17.
 */
public class FBConnection implements ApiConnection {

    private UserBean owner;

    public FBConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {

    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        Message textMessage = new Message(message);
        FacebookClient pageClient = new DefaultFacebookClient(owner.getAccessToken(), Version.VERSION_2_8);
        IdMessageRecipient recipient = new IdMessageRecipient(contact.getId());

        try {
            pageClient.publish("me/messages", SendResponse.class,
                    Parameter.with("recipient", recipient),
                    Parameter.with("message", textMessage));
        } catch (Exception e) {
            e.printStackTrace();
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from FBConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());
        }

    }

    @Override
    public void closeSession() {

    }
}
