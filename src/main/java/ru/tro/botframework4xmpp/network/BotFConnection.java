package ru.tro.botframework4xmpp.network;

import clientapi.botframeworkclient.model.*;
import com.google.gson.Gson;
import com.squareup.okhttp.*;
import clientapi.botframeworkclient.ApiClient;
import clientapi.botframeworkclient.ApiException;
import clientapi.botframeworkclient.api.ConversationsApi;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.TokenBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.util.logging.Logger;

/**
 * Created by aleksey on 29.01.17.
 */
public class BotFConnection implements ApiConnection {
    private static final Logger _log = Logger.getLogger(BotFConnection.class.getName());
    private TokenExtenderThread thread;
    private UserBean owner;
    private ApiClient client = new ApiClient();

    public BotFConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {
        closeSession();
        thread = new TokenExtenderThread();
        thread.start();

    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        try {
            Activity activity = new Activity();
            activity.setType("message");
            //activity.setChannelId(owner.getChannel().name());
            activity.setFrom(new ChannelAccount().id(owner.getChannelId()).name(owner.getChannelName()));
            activity.setRecipient(new ChannelAccount().id(contact.getId()).name(contact.getName()));
            client.setAccessToken(owner.getAccessToken());

            //card test
            if (message.equalsIgnoreCase("dk")) {
                Attachment attachment = new Attachment();
                attachment.setContentType("application/vnd.microsoft.card.hero");
                HeroCard content = new HeroCard();
                content.setTitle("Привет, Дима");
                content.setSubtitle("Ну как?");
                CardImage image = new CardImage();
                image.setUrl("https://pp.userapi.com/c836436/v836436136/2e81b/7jlZekmkjeE.jpg");
                image.setAlt("dimon");
                content.getImages().add(image);

                CardAction button1 = new CardAction();
                button1.setType("imBack");
                button1.setTitle("Превосходно");

                CardAction button2 = new CardAction();
                button2.setType("imBack");
                button2.setTitle("Пойду продавать это!");

                CardAction button3 = new CardAction();
                button3.setType("openUrl");
                button3.setTitle("Хочу на сайт!");

                content.getButtons().add(button1);
                content.getButtons().add(button2);
                content.getButtons().add(button3);
                attachment.setContent(content);
                activity.getAttachments().add(attachment);

            } else {
                activity.setText(message);
            }
            client.setBasePath(owner.getServiceUrl());
            new ConversationsApi(client).conversationsSendToConversation(activity, contact.getConversationId());
        } catch (ApiException e) {
            e.printStackTrace();
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from BotFConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());

        }
    }

    @Override
    public void closeSession() {
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
        }
    }

    private class TokenExtenderThread extends Thread {
        @Override
        public void run() {
            long lastUpdate = 0;
            long expiresIn = 3600000;

            while (true) {
                try {
                    Thread.sleep(1000);
                    if (lastUpdate + expiresIn <= System.currentTimeMillis() + 10000) {
                    OkHttpClient client = new OkHttpClient();

                    RequestBody requestBody = new MultipartBuilder()
                            .type(MultipartBuilder.FORM)
                            .addFormDataPart("client_id", owner.getAppId())
                            .addFormDataPart("client_secret", owner.getAppSecret())
                            .addFormDataPart("grant_type", "client_credentials")
                            .addFormDataPart("scope", "https://graph.microsoft.com/.default")
                            .build();

                    Request request = new Request.Builder()
                            .url("https://login.microsoftonline.com/common/oauth2/v2.0/token")
                            .method("POST", RequestBody.create(null, new byte[0]))
                            .post(requestBody)
                            .build();

                    try {
                        String response = client.newCall(request).execute().body().string();
                        TokenBean token = new Gson().fromJson(response, TokenBean.class);
                        _log.info("User: " + owner.getJid() + "\nToken: " + response);
                        expiresIn = token.getExpiresIn();
                        owner.setAccessToken(token.getAccess_token());
                        lastUpdate = System.currentTimeMillis();
                    } catch (Exception e) {
                        e.printStackTrace();
                        XMPPMessage.sendWarning(owner.getChannel(), "Error while refreshing token from BotFConnection for user " + owner.getJid() + "\n" +
                                e.getMessage());

                        Thread.sleep(20000);
                    }
                } else {
                    try {
                        Thread.sleep((lastUpdate + expiresIn) - System.currentTimeMillis() - 10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
