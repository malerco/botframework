package ru.tro.botframework4xmpp.network;


import clientapi.wechatclient.WeChatClient;
import clientapi.wechatclient.WeChatTokenExtender;
import clientapi.wechatclient.model.WeChatMessage;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.io.IOException;

/**
 * Created by ivametal on 16/06/17.
 */
public class WeChatConnection implements ApiConnection {
    private UserBean owner;
    private WeChatTokenExtender tokenExtender;

    public WeChatConnection(UserBean userBean) {
        this.owner = userBean;
    }


    @Override
    public void init() {
        closeSession();
        tokenExtender = new WeChatTokenExtender(owner);
        tokenExtender.start();
    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        WeChatMessage weChatMessage = new WeChatMessage(contact.getId(), WeChatMessage.MessageType.text, message);
        try {
            new WeChatClient(owner.getAccessToken()).sendMessage(weChatMessage);
        }
        catch (IOException e) {
            e.printStackTrace();
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from WeChatConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());

        }
    }

    @Override
    public void closeSession() {

        if (tokenExtender != null && tokenExtender.isAlive()) {
            tokenExtender.interrupt();
        }
    }

}