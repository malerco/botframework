package ru.tro.botframework4xmpp.network;

import clientapi.viberpublic.ViberPublicClient;
import clientapi.viberpublic.model.message.Message;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;

import java.io.IOException;

/**
 * Created by aleksey on 07.05.17.
 */
public class ViberPublicConnection implements ApiConnection {

    private UserBean owner;

    public ViberPublicConnection(UserBean userBean) {
        owner = userBean;
    }

    @Override
    public void init() {

    }

    @Override
    public void sendMessage(ContactBean contact, String message) {
        Message msg = new Message(contact.getConversationId(), message);
        try {
            new ViberPublicClient(owner.getAccessToken()).sendMessage(msg);
        } catch (IOException e) {
            XMPPMessage.sendWarning(owner.getChannel(), "Error while sending message from ViberPublicConnection for user " + owner.getJid() + "\n" +
                    e.getMessage());

       }
    }

    @Override
    public void closeSession() {

    }
}
