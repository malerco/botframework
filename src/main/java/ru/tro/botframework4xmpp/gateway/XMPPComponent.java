package ru.tro.botframework4xmpp.gateway;

import org.xmpp.component.Component;
import org.xmpp.component.ComponentException;
import org.xmpp.component.ComponentManager;
import org.xmpp.component.ComponentManagerFactory;
import org.xmpp.packet.*;
import org.xmpp.packet.Presence.Type;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.handlers.PresenceHandler;
import ru.tro.botframework4xmpp.packets.XMPPIQ;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.packets.XMPPPresence;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Класс создающий подключение к джабер серверу
 * @author TANT
 *
 */
public class XMPPComponent implements Component {

	private static final Logger _log = Logger.getLogger(XMPPComponent.class.getName());
	private Server.CHANNEL channel;

	public XMPPComponent(Server.CHANNEL channel) {
		this.channel = channel;
	}
	
	@Override
	public String getName() {
		return channel.name() + " Component";
	}

	@Override
	public String getDescription() {
		return channel.name() + " Component";
	}

	@Override
	public void processPacket(Packet packet)
	{
		if (Config.DEBUG)
		{
			System.out.println("Received package" + packet.toString() + "\r");
			_log.info("Received package: " + packet.toString() + "\r");
		}
		if (Config.XML_LOG)
		{
			_log.logp(Level.INFO, this.getName(), "xml", "<received>" + packet.toString() + Config.EOL + "</received>");
		}
		
		if (packet instanceof Presence)
		{
			XMPPPresence.process(packet);
		}
		else if (packet instanceof IQ)
		{
			XMPPIQ.process(packet);
		}
		else if (packet instanceof Message)
		{
			XMPPMessage.process(packet);
		}
	}
	
	public void sendPackets(ArrayList<Packet> replay)
	{
		long time = 200;
		if (replay.size()>20) time = 500;
		if (!replay.isEmpty())
		{
			for (Packet pac : replay)
			{
				if (Config.DEBUG)
				{
					System.out.println("Send package:" + pac.toString() + "\r");
					_log.info("Send package: " + pac.toString() + "\r");
				}
				if (Config.XML_LOG)
				{
					_log.logp(Level.INFO, this.getName(), "xml", "<send>" + pac.toString() + Config.EOL + "</send>");
				}
				
				try 
				{
					Thread.sleep(time);
					ComponentManagerFactory.getComponentManager().sendPacket(this, pac);
				} 
				catch (Exception e) 
				{
					_log.info("Error in Component.sendPackets(): " + e.getMessage());
				}
			}
		}
	}
	
	public void sendPacket(Packet packet)
	{	
		if (Config.DEBUG)
		{
			System.out.println("Send package:" + packet.toString() + "\r");
			_log.info("Send package: " + packet.toString() + "\r");
		}
		if (Config.XML_LOG)
		{
			_log.logp(Level.INFO, this.getName(), "xml", "<send>" + packet.toString() + Config.EOL + "</send>");
		}
				
		try 
		{
			ComponentManagerFactory.getComponentManager().sendPacket(this, packet);
		} 
		catch (Exception e) 
		{
			_log.info("Error in Component.sendPacket(): " + e.getMessage());
		}
	}

	@Override
	public void initialize(JID jid, ComponentManager componentManager) throws ComponentException {}

	@Override
	public void start() {}

	@Override
	public void shutdown() 
	{
		Users.getInstance().stream().filter(UserBean::isOnline).forEach(user -> {
			user.setOnline(false);
			ArrayList<Packet> toSend = Contacts.getInstance().findAllByOwner(user.getChannel(), user.getJid())
					.stream()
					.map(cont -> PresenceHandler.genStatus(user, cont.getJid(), Type.unavailable))
					.collect(Collectors.toCollection(ArrayList::new));

			toSend.add(PresenceHandler.genStatus(user, channel.getWithDomain(), Type.unavailable));
			sendPackets(toSend);
		});
	}
}
