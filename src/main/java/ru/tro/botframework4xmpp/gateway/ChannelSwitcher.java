package ru.tro.botframework4xmpp.gateway;

import org.jivesoftware.whack.ExternalComponentManager;
import ru.tro.botframework4xmpp.Server.CHANNEL;
import ru.tro.botframework4xmpp.beans.GatewayBean;

/**
 * Created by vishnyakov on 23.03.17.
 */
public class ChannelSwitcher {
    private static ChannelSwitcher ourInstance = new ChannelSwitcher();

    public static ChannelSwitcher getInstance() {
        return ourInstance;
    }

    private ChannelSwitcher() {
    }

    private XMPPGateway skypeGate = new XMPPGateway(CHANNEL.skype);
    private XMPPGateway facebookGate = new XMPPGateway(CHANNEL.facebook);
    private XMPPGateway slackGate = new XMPPGateway(CHANNEL.slack);
    private XMPPGateway kikGate = new XMPPGateway(CHANNEL.kik);
    private XMPPGateway okruGate = new XMPPGateway(CHANNEL.okru);
    private XMPPGateway vkGate = new XMPPGateway(CHANNEL.vk);
    private XMPPGateway wechatGate = new XMPPGateway(CHANNEL.wechat);
    private XMPPGateway fbGate = new XMPPGateway(CHANNEL.fb);
    private XMPPGateway telegramGate = new XMPPGateway(CHANNEL.telegram2);
    private XMPPGateway viberpublicGate = new XMPPGateway(CHANNEL.viberpublic);
    private XMPPGateway telegrambotGate = new XMPPGateway(CHANNEL.telegram3);

    public void init() {
        skypeGate.start();
        facebookGate.start();
        slackGate.start();
        kikGate.start();
        okruGate.start();
        vkGate.start();
        wechatGate.start();
        fbGate.start();
        telegramGate.start();
        viberpublicGate.start();
        telegrambotGate.start();
    }

    public ExternalComponentManager getManager(CHANNEL channel) {
        switch (channel) {
            case facebook:
                return facebookGate.getManager();

            case skype:
                return skypeGate.getManager();

            case slack:
                return slackGate.getManager();

            case kik:
                return kikGate.getManager();

            case okru:
                return okruGate.getManager();

            case vk:
                return vkGate.getManager();

            case wechat:
                return wechatGate.getManager();

            case fb:
                return fbGate.getManager();

            case telegram2:
                return telegramGate.getManager();

            case viberpublic:
                return viberpublicGate.getManager();

            case telegram3:
                return telegrambotGate.getManager();

        }
        return null;
    }

    public XMPPComponent getComponent(CHANNEL channel) {
        switch (channel) {
            case facebook:
                return facebookGate.getComponent();

            case skype:
                return skypeGate.getComponent();

            case slack:
                return slackGate.getComponent();

            case kik:
                return kikGate.getComponent();

            case okru:
                return okruGate.getComponent();

            case vk:
                return vkGate.getComponent();

            case wechat:
                return wechatGate.getComponent();

            case fb:
                return fbGate.getComponent();

            case telegram2:
                return telegramGate.getComponent();

            case viberpublic:
                return viberpublicGate.getComponent();

            case telegram3:
                return telegrambotGate.getComponent();


        }
        return null;
    }

    public GatewayBean getServ(CHANNEL channel) {
        switch (channel) {
            case facebook:
                return facebookGate.getServ();

            case skype:
                return skypeGate.getServ();

            case slack:
                return slackGate.getServ();

            case kik:
                return kikGate.getServ();

            case okru:
                return okruGate.getServ();

            case vk:
                return vkGate.getServ();

            case wechat:
                return wechatGate.getServ();

            case fb:
                return fbGate.getServ();

            case telegram2:
                return telegramGate.getServ();

            case viberpublic:
                return viberpublicGate.getServ();

            case telegram3:
                return telegrambotGate.getServ();
        }
        return null;
    }

    public void shutdown() {
        if (skypeGate != null && skypeGate.isAlive()) {
            skypeGate.interrupt();
        }
        if (facebookGate != null && facebookGate.isAlive()) {
            facebookGate.interrupt();
        }
        if (slackGate != null && slackGate.isAlive()) {
            slackGate.interrupt();
        }
        if (kikGate != null && kikGate.isAlive()) {
            kikGate.interrupt();
        }
        if (okruGate != null && okruGate.isAlive()) {
            okruGate.interrupt();
        }
        if (vkGate != null && vkGate.isAlive()) {
            vkGate.interrupt();
        }
        if (wechatGate != null && wechatGate.isAlive()) {
            wechatGate.interrupt();
        }
        if (fbGate != null && fbGate.isAlive()) {
            fbGate.interrupt();
        }
        if (telegramGate != null && telegramGate.isAlive()) {
            telegramGate.interrupt();
        }
        if (viberpublicGate != null && viberpublicGate.isAlive()) {
            viberpublicGate.interrupt();
        }
        if (telegrambotGate != null && telegrambotGate.isAlive()) {
            telegrambotGate.interrupt();
        }
    }
}
