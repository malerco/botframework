package ru.tro.botframework4xmpp.gateway;

import org.jivesoftware.whack.ExternalComponentManager;
import org.xmpp.component.ComponentException;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.GatewayBean;
import ru.tro.botframework4xmpp.config.Config;

/**
 * Created by aleksey on 29.01.17.
 */
class XMPPGateway extends Thread {

    private GatewayBean serv = new GatewayBean();
    private XMPPComponent component;
    private Server.CHANNEL channel;
    private static final ExternalComponentManager manager = new ExternalComponentManager(Config.XMPP_HOST,	Config.XMPP_PORT);

    public XMPPGateway (Server.CHANNEL channel) {
        this.channel = channel;
        component = new XMPPComponent(channel);
    }

    @Override
    public void run()
    {
        manager.setSecretKey(channel.name(), Config.XMPP_SALT);
        manager.setMultipleAllowed(channel.name(), true);

        try
        {
            // Register that this component will be serving the given subdomain
            // of the server
            manager.addComponent(channel.name(), component);
            // Quick trick to ensure that this application will be running for
            // ever. To stop the
            // application you will need to kill the process
            while (true)
            {
                try
                {
                    Thread.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        catch (ComponentException e)
        {
            e.printStackTrace();
        }
    }

    public ExternalComponentManager getManager()
    {
        return manager;
    }

    public GatewayBean getServ() {
        return serv;
    }

    public XMPPComponent getComponent() {
        return component;
    }
}
