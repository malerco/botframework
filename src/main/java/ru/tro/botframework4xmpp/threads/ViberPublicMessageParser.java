package ru.tro.botframework4xmpp.threads;

import clientapi.viberpublic.model.message.ViberPublicMessageBean;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Created by ivametal on 14/06/17.
 */
public class ViberPublicMessageParser extends Thread {

    private String data;
    private ViberPublicMessageBean message;

    public ViberPublicMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        String eventType = new JsonParser().parse(data).getAsJsonObject().get("event").getAsString();

        if (!eventType.equals("message")) {
            return;
        }

        try {
            message = new Gson().fromJson(data, ViberPublicMessageBean.class);
        }
        catch (Error e) {
            e.printStackTrace();
            return;
        }

        if (message == null)
            return;


        Server.CHANNEL CHNL = Server.CHANNEL.viberpublic;

        UserBean user = Users.getInstance().findUserByJid(CHNL, message.getOwner_jid());
        if (user == null) {
            return;
        }

        String contactJid = message.getSender().getId() + CHNL.getWithAt();
        ContactBean contact = Contacts.getInstance().find(contactJid, user.getId());


        if (contact == null) {
                contact = new ContactBean(contactJid, user.getJid(), user.getId(), message.getSender().getId(),
                        message.getSender().getName(), message.getSender().getId(), CHNL);

                Contacts.getInstance().tryInsert(CHNL, contact);
        }

        XMPPMessage.sendMessageFromViberPublic(user, contactJid, message);

    }
}
