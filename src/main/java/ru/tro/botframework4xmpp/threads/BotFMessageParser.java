package ru.tro.botframework4xmpp.threads;

import clientapi.botframeworkclient.model.BotFMessageBean;
import com.google.gson.Gson;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Created by aleksey on 20.05.17.
 */
public class BotFMessageParser extends Thread {
    private String data;

    public BotFMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        BotFMessageBean message;
        try {
            message = new Gson().fromJson(data, BotFMessageBean.class);
        } catch (Exception e) {
            return;
        }

        if (message != null && message.getType() != null && message.getType().equals("message")) {

            Server.CHANNEL CHNL = Server.CHANNEL.valueOf(message.getChannelId());
            //ищем - кому отправлять сообщение

            UserBean user;

            if (message.getOwnerJid() != null && !message.getOwnerJid().isEmpty()) {
                user = Users.getInstance().findUserByJid(CHNL, message.getOwnerJid());

            } else {
                user = Users.getInstance().findUserByChannelId(message.getRecipient().getId());

            }

            if (user == null) {
                return;
            }

            //сохраняем service url и channel_name
            user.updateServiceUrl(message.getServiceUrl());
            user.updateChannelName(message.getRecipient().getName());

            //ищем контакт, от имени которого отправлять. Если не найдем - создаем
            String contactJid = message.getConversation().getId()
                    .replaceAll("\\W", "") + CHNL.getWithAt();
            ContactBean contact = Contacts.getInstance().find(contactJid, user.getId());

            //если контакта нет - создаем
            if (contact == null) {

                contact = new ContactBean(contactJid, user.getJid(),
                        user.getId(), message.getFrom().getId(), message.getFrom().getName(), message.getConversation().getId(), CHNL);
                Contacts.getInstance().tryInsert(CHNL, contact);
            }

            //отправляем сообщение
            XMPPMessage.sendMessageFromBotFramework(user, contact.getJid(), message);
        }
    }
}
