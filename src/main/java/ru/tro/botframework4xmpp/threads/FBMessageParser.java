package ru.tro.botframework4xmpp.threads;

import com.restfb.*;
import com.restfb.types.User;
import com.restfb.types.webhook.WebhookEntry;
import com.restfb.types.webhook.WebhookObject;
import com.restfb.types.webhook.messaging.MessagingItem;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Created by aleksey on 20.05.17.
 */
public class FBMessageParser extends Thread {
    private String data;

    public FBMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        JsonMapper mapper = new DefaultJsonMapper();
        WebhookObject webhookObject =
                mapper.toJavaObject(data, WebhookObject.class);

        Server.CHANNEL CHNL = Server.CHANNEL.fb;
        //ищем - кому отправлять сообщение
        for (WebhookEntry entry : webhookObject.getEntryList()) {

            UserBean user = Users.getInstance().findUserByChannelId(entry.getId());
            if (user == null) {
                return;
            }

            //ищем контакт, от имени которого отправлять. Если не найдем - создаем
            MessagingItem message = entry.getMessaging().get(0);
            String contactJid = message.getSender().getId().replaceAll("\\W", "") + CHNL.getWithAt();
            ContactBean contact = Contacts.getInstance().find(contactJid, user.getId());

            //если контакта нет - создаем
            if (contact == null) {
                FacebookClient facebookClient = new DefaultFacebookClient(user.getAccessToken(), Version.VERSION_2_9);
                User fbUser = facebookClient.fetchObject(message.getSender().getId(), User.class);
                String name = fbUser.getFirstName() + " " + fbUser.getLastName();
                contact = new ContactBean(contactJid, user.getJid(),
                        user.getId(), message.getSender().getId(), name,
                        message.getSender().getId(), CHNL);
                Contacts.getInstance().tryInsert(CHNL, contact);
            }

            //отправляем сообщение
            XMPPMessage.sendMessageFromFB(user, contact.getJid(), message);
        }
    }
}
