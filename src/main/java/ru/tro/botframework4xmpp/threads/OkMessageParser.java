package ru.tro.botframework4xmpp.threads;

import clientapi.okruclient.model.message.OkRuMessageBean;
import com.google.gson.Gson;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Created by aleksey on 20.05.17.
 */
public class OkMessageParser extends Thread {
    private String data;

    public OkMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        OkRuMessageBean message;
        try {
            message = new Gson().fromJson(data, OkRuMessageBean.class);
        } catch (Exception e) {
            return;
        }

        if (message == null) {
            return;
        }

        Server.CHANNEL CHNL = Server.CHANNEL.okru;
        //ищем - кому отправлять сообщение

        UserBean user = Users.getInstance().findUserByJid(CHNL, message.getOwnerJid());
        if (user == null) {
            return;
        }

        //ищем контакт, от имени которого отправлять. Если не найдем - создаем
        String contactJid = message.getRecipient().getChatId().replaceAll("\\W", "") + CHNL.getWithAt();
        ContactBean contact = Contacts.getInstance().find(contactJid, user.getId());

        //если контакта нет - создаем
        if (contact == null) {
            contact = new ContactBean(contactJid, user.getJid(),
                    user.getId(), message.getSender().getUserId(), message.getSender().getName(),
                    message.getRecipient().getChatId(), CHNL);
            Contacts.getInstance().tryInsert(CHNL, contact);
        }

        //отправляем сообщение
        XMPPMessage.sendMessageFromOkRu(user, contact.getJid(), message);
    }
}
