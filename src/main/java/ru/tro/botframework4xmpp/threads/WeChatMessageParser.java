package ru.tro.botframework4xmpp.threads;

import clientapi.wechatclient.model.WeChatMessage;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.io.IOException;

/**
 * Created by ivametal on 14/06/17.
 */
public class WeChatMessageParser extends Thread {

    private String data;

    public WeChatMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        WeChatMessage message;
        try {
            message = new Gson().fromJson(data, WeChatMessage.class);
        }
        catch (Error e) {
            e.printStackTrace();
            return;
        }

        if (message == null)
            return;

        Server.CHANNEL CHNL = Server.CHANNEL.wechat;

        UserBean user = Users.getInstance().findUserByJid(CHNL, message.getOwner_jid());
        if (user == null) {
            return;
        }

        String contactJid = message.getFromuser() + CHNL.getWithAt();
        ContactBean contact = Contacts.getInstance().find(contactJid, user.getId());


        if (contact == null) {
            String url = "https://api.wechat.com/cgi-bin/user/info?access_token="+user.getAccessToken()+
                    "&openid="+ message.getFromuser()+"&lang=en_US";
            Request request = new Request.Builder()
                    .url(url).build();
            try {
                Response response = new OkHttpClient().newCall(request).execute();
                String answer = response.body().string();
                String nickName = new JsonParser().parse(answer).getAsJsonObject().get("nickname").getAsString();

                contact = new ContactBean(contactJid, user.getJid(), user.getId(), message.getFromuser(), nickName, message.getFromuser(), CHNL);

                Contacts.getInstance().tryInsert(CHNL, contact);
            } catch (IOException e) {
                e.printStackTrace();
                XMPPMessage.sendWarning(user.getChannel(), "Error in 'WechatMessageParser' for user " + user.getJid() + "\n" +
                        e.getMessage());
            }

        }

        XMPPMessage.sendMessageFromWechat(user, contactJid, message);

    }
}
