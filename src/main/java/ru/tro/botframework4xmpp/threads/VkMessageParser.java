package ru.tro.botframework4xmpp.threads;

import clientapi.vkclient.model.WebHookObject;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.packets.XMPPMessage;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.io.IOException;

/**
 * Created by aleksey on 20.05.17.
 */
public class VkMessageParser extends Thread{
    private String data;

    public VkMessageParser(String data) {
        this.data = data;
    }

    @Override
    public void run() {
        setName(this.getClass().getSimpleName());
        WebHookObject message;
        try {
            message = new Gson().fromJson(data, WebHookObject.class);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (message == null) {
            return;
        }

        Server.CHANNEL CHNL = Server.CHANNEL.vk;
        //ищем - кому отправлять сообщение
        UserBean user = Users.getInstance().findUserByJid(CHNL, message.getOwnerJid());
        if (user == null) {
            return;
        }

        //ищем контакт, от имени которого отправлять. Если не найдем - создаем
        String contactJid = message.getMessage().getUserId() + CHNL.getWithAt();
        ContactBean  contact = Contacts.getInstance().find(contactJid, user.getId());

        //если контакта нет - создаем
        if (contact == null) {

            //парсим имя контакта через ВК АПИ
            Request request = new Request.Builder()
                    .url("https://api.vk.com/method/users.get?user_ids=" + message.getMessage().getUserId() + "&v=5.60")
                    .build();
            try {
                Response response = new OkHttpClient().newCall(request).execute();
                String answer = response.body().string();
                String firstName = new JsonParser().parse(answer)
                        .getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject().get("first_name").getAsString();
                String lastName = new JsonParser().parse(answer)
                        .getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject().get("last_name").getAsString();

                //создаем контакт
                contact = new ContactBean(contactJid, user.getJid(),
                        user.getId(), message.getMessage().getUserId(), firstName + " " + lastName,
                        message.getMessage().getUserId(), CHNL);
                Contacts.getInstance().tryInsert(CHNL, contact);
            } catch (IOException e) {
                e.printStackTrace();
                XMPPMessage.sendWarning(user.getChannel(), "Error in 'VkMessageParser' for user " + user.getJid() + "\n" +
                        e.getMessage());
            }
        }

        //отправляем сообщение
        XMPPMessage.sendMessageFromVk(user, contactJid, message.getMessage());

    }
}
