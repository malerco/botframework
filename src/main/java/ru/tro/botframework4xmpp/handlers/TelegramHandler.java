package ru.tro.botframework4xmpp.handlers;

import com.github.badoualy.telegram.api.TelegramApp;

import java.io.File;

public class TelegramHandler {

    // Get them from Telegram's console
    private static final int API_ID = 90401;
    private static final String API_HASH = "3da6f0e1f217cc12bb759053ef2487fb";

    // What you want to appear in the "all sessions" screen
    private static final String APP_VERSION = "v1.0";
    private static final String MODEL = "MySender Transport";
    private static final String SYSTEM_VERSION = "v1.0";
    private static final String LANG_CODE = "en";

    public static TelegramApp application = new TelegramApp(API_ID, API_HASH, MODEL, SYSTEM_VERSION, APP_VERSION, LANG_CODE);

    public static final File CLIENTS_DIR = new File("telegram_storage" + File.separator + "clients" + File.separator);
    public static final File MEDIA_DIR = new File("telegram_storage" + File.separator + "media" + File.separator);

    static {
        if (!CLIENTS_DIR.exists()) {
            CLIENTS_DIR.mkdirs();
        }

        if (!MEDIA_DIR.exists()) {
            MEDIA_DIR.mkdirs();
        }
    }

    public static boolean hasTelegramAuth(String phone) {
        return new File(CLIENTS_DIR + File.separator + phone, "auth.complete").exists();
    }

    public static boolean deleteTelegramAuth(String phone) {
        return new File(CLIENTS_DIR + File.separator + phone, "auth.complete").delete();
    }


}
