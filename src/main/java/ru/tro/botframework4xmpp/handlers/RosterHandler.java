package ru.tro.botframework4xmpp.handlers;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.xmpp.packet.IQ.Type;
import org.xmpp.packet.*;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.extensions.ExtChannelExtension;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RosterHandler {
	private static final Logger _log = Logger.getLogger(RosterHandler.class.getName());

	/**
	 * Функция генерирует пакет типа Roster по инстансу пользователя
	 * Передаёт имя пользователя, его JID и в какую группу его записать

	 * @param _group
	 * @param sub
	 * @return
	 */
	public static Packet genRoster(ContactBean contactBean, String _group, String sub)
	{
		UserBean owner = Users.getInstance().findUserById(contactBean.getOwnerId());
		Roster ro = new Roster(Type.set, ChannelSwitcher.getInstance().getServ(owner.getChannel()).getId());
		ro.setTo(owner.getFullJid());
		Element query2 = DocumentHelper.createElement("query");
		Element item = DocumentHelper.createElement("item");
		item.addAttribute("jid", contactBean.getJid().toLowerCase());
		item.addAttribute("name", contactBean.getName());
		item.addAttribute("subscription", sub);
		Element group = DocumentHelper.createElement("group");
		group.setText(_group);
		item.add(group);
		Element extchannel = DocumentHelper.createElement(ExtChannelExtension.NAME);
		extchannel.setText(owner.getLogin());
		item.add(extchannel);
		query2.add(item);
		NameSpaceChanger.setNamespaces(query2, DocumentHelper.createNamespace("", "jabber:iq:roster"));
		ro.setChildElement(query2);
		
		return ro;
	}

	public static Packet genRoster(UserBean user, String withDomain, String name, String transports, String from) {
		Roster ro = new Roster(Type.set, ChannelSwitcher.getInstance().getServ(user.getChannel()).getId());
		ro.setTo(user.getFullJid());
		Element query2 = DocumentHelper.createElement("query");
		Element item = DocumentHelper.createElement("item");
		item.addAttribute("jid", withDomain.toLowerCase());
		item.addAttribute("name", name);
		item.addAttribute("subscription", from);
		Element group = DocumentHelper.createElement("group");
		group.setText(transports);
		item.add(group);
		query2.add(item);
		NameSpaceChanger.setNamespaces(query2, DocumentHelper.createNamespace("", "jabber:iq:roster"));
		ro.setChildElement(query2);

		return ro;	}


	/**
	 * Функция обрабатывает входящие запросы типа <query xmlns="jabber:iq:roster">
	 * @param packet
	 */
	public static void process(Packet packet)
	{
		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}
		String subscription = packet.getElement().element("query").element("item").attributeValue("subscription");

	}
}
