package ru.tro.botframework4xmpp.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dom4j.Element;
import org.xmpp.packet.IQ;
import org.xmpp.packet.Packet;
import org.xmpp.packet.Presence.Type;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

/**
 * Класс обрабатывающий пакеты типа result
 * @author TANT
 *
 */
public class ResultHandler {
	private static final Logger _log = Logger.getLogger(ResultHandler.class.getName());

	@SuppressWarnings("unchecked")
	public static void process(Packet packet) {
		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}

		IQ iq = (IQ) packet;
		UserBean user = Users.getInstance().findUserByJid(CHNL, iq.getFrom().toBareJID());

		if (user == null)
			return;

		user.setResource(packet.getFrom().getResource());

		if (iq.getID().startsWith("getUsers"))
		{
			ArrayList<Packet> result = new ArrayList<>();
			ArrayList<Element> items = (ArrayList<Element>) iq.getElement().element("query").elements();
			HashMap<String, String> itemsJid = new HashMap<>();
			
			if(items != null && items.size()>0)
			{
				for (Element item : items)
				{
					itemsJid.put(item.attributeValue("jid"), "");
				}
			}
			
			result.add(PresenceHandler.genStatus(user, user.getChannel().getWithDomain(), null));
			
			Type status = null;

			//TODO проверка на включенный АПИ
			/*if (user.getConnection() == null || (user.getConnection() != null && !user.getConnection().isAlive()))
				status = Type.unavailable;
			*/
			boolean isOnline = true;


			for (ContactBean cont : Contacts.getInstance().findAllByOwner(CHNL, user.getJid()))
			{
				String userJID = user.getChannel().getWithResource(cont.getJid());
				if (itemsJid.containsKey(userJID))
				{
					cont.setOnline(true);
					result.add(PresenceHandler.genStatus(user, userJID, status));
				}
				else
				{
					Packet ro = RosterHandler.genRoster(user, userJID, cont.getName(), user.getChannel().name(), "to");
                    result.add(ro);
					cont.setOnline(isOnline);
					result.add(PresenceHandler.genStatus(user, userJID, status));
				}
			}
            Packet transport = RosterHandler.genRoster(user, CHNL.getWithDomain(), CHNL.name(), "Transports", "from");
            result.add(transport);

			//TODO Включаем апи
			user.setOnline(true);
			ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPackets(result);
		}
		else if (iq.getID().startsWith("isonline"))
		{
			ChannelSwitcher.getInstance().getComponent(user.getChannel()).sendPacket(PresenceHandler.genStatus(user, user.getChannel().getWithDomain(), null));
		}
	}

}
