package ru.tro.botframework4xmpp.handlers;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.xmpp.packet.IQ;
import org.xmpp.packet.Packet;
import org.xmpp.packet.IQ.Type;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.singleton.Users;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DiscoInfoHandler 
{
	private static final Logger _log = Logger.getLogger(DiscoInfoHandler.class.getName());

	/**
	 * ������� ��������������� ������ ���� <query xmlns="http://jabber.org/protocol/disco#info"/>
	 * @param packet
	 */
	public static void process(Packet packet)
	{
		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}
		if(packet.getFrom().toString().equals("component."+ Config.XMPP_HOST))
		{
			ChannelSwitcher.getInstance().getServ(CHNL).setId(packet.getID());
			ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(gen(CHNL, packet, packet.getID()));
			if (!Users.getInstance().isEmpty())
			{
				for (UserBean user : Users.getInstance())
				{
					IQ ping = new IQ(Type.get, "isonline"+user.getAppId());
					ping.setTo(user.getJid());
					ping.setFrom(CHNL.getWithDomain());
					ping.setChildElement("ping", "urn:xmpp:ping");
					ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(ping);
				}
			}
		}
		else if (packet.getFrom().toString().startsWith(Config.XMPP_HOST))
		{
			IQ iq = new IQ(Type.result, packet.getID());
			iq.setTo(packet.getFrom());
			iq.setFrom(packet.getTo());
			Element query = (Element) packet.getElement().element("query").detach();
			query.clearContent();
			
			Element item = DocumentHelper.createElement("item");
			item.addAttribute("jid", CHNL.getWithDomain());
			item.addAttribute("node", "Transports");
			item.addAttribute("name", CHNL.name());
			
			query.add(item);
			iq.setChildElement(query);
			NameSpaceChanger.setNamespaces(query, DocumentHelper.createNamespace("", "http://jabber.org/protocol/disco#info"));
			
			ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(iq);
		}
		else
		{
			ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(gen(CHNL, packet, packet.getID()));
			String userBaseJid = packet.getFrom().toBareJID();
			String resource = packet.getFrom().getResource();
			if (!Users.getInstance().isEmpty() && Users.getInstance().containsJid(CHNL, userBaseJid))
			{
				UserBean user = Users.getInstance().findUserByJid(CHNL, userBaseJid);
				user.setResource(resource);
				user.setOnline(true);
				IQ ro = new IQ(Type.get, "getUsers"+user.getAppId());
				ro.setTo(packet.getFrom());
				ro.setFrom(packet.getTo());
				ro.setChildElement("query", "jabber:iq:roster");
				
				ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(ro);
			}
		}
	}

	/**
	 * ������� ��������� ����� ��� ������� ��� ������ ������ � ���������� 
	 * @param packet
	 * @param id
	 * @return
	 */
	private static Packet gen(Server.CHANNEL channel, Packet packet, String id)
	{
		IQ iq = new IQ(Type.result, id);
		iq.setTo(packet.getFrom());
		iq.setFrom(packet.getTo());
		Element query = (Element) packet.getElement().element("query").detach();
		query.clearContent();
		
		Element identity = DocumentHelper.createElement("identity");
		identity.addAttribute("category", "gateway");
		identity.addAttribute("type", channel.name());
		identity.addAttribute("name", channel.name());
		query.add(identity);
		
		Element feature = DocumentHelper.createElement("feature");
		feature.addAttribute("var", "http://jabber.org/protocol/disco#info");
		query.add(feature);
		
		Element feature2 = DocumentHelper.createElement("feature");
		feature2.addAttribute("var", "http://jabber.org/protocol/disco#items");
		query.add(feature2);
		
		Element feature3 = DocumentHelper.createElement("feature");
		feature3.addAttribute("var", "jabber:iq:gateway");
		query.add(feature3);
		
		Element feature31 = DocumentHelper.createElement("feature");
		feature31.addAttribute("var", "jabber:iq:roster");
		query.add(feature31);
		
		Element feature4 = DocumentHelper.createElement("feature");
		feature4.addAttribute("var", "jabber:iq:register");
		query.add(feature4);
		
		Element feature5 = DocumentHelper.createElement("feature");
		feature5.addAttribute("var", "jabber:iq:version");
		query.add(feature5);
		
		Element feature6 = DocumentHelper.createElement("feature");
		feature6.addAttribute("var", "jabber:iq:last");
		query.add(feature6);
		
		Element feature8 = DocumentHelper.createElement("feature");
		feature8.addAttribute("var", "jabber:iq:registered");
		query.add(feature8);

		iq.setChildElement(query);
		NameSpaceChanger.setNamespaces(query, DocumentHelper.createNamespace("", "http://jabber.org/protocol/disco#info"));
		
		return iq;
	}
}
