package ru.tro.botframework4xmpp.handlers;

import org.xmpp.packet.JID;
import org.xmpp.packet.Presence;
import org.xmpp.packet.Presence.Type;

import ru.tro.botframework4xmpp.beans.UserBean;

public class PresenceHandler
{
	/**
	 * Функция генерирует пакет типа Presence по инстансу пользователя и имени отправителя
	 * @param user
	 * @param from
	 * @param type
	 * @return
	 */
	public static Presence genStatus(UserBean user, String from, Type type)
	{
		Presence pres = new Presence();
		pres.setTo(user.getJid());
		pres.setFrom(from);
		if (type != null)
		{
			pres.setType(type);
		}
		return pres;
	}
	
	/**
	 * Функция генерирует пакет типа Presence по JID отправителя и получателя
	 * @param to
	 * @param from
	 * @param type
	 * @return
	 */
	public static Presence genStatus(JID to, JID from, Type type)
	{
		Presence pres = new Presence();
		pres.setTo(to);
		pres.setFrom(from);
		if (type != null)
		{
			pres.setType(type);
		}
		return pres;
	}
}
