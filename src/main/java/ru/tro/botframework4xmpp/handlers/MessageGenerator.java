package ru.tro.botframework4xmpp.handlers;

import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Message.Type;
import org.xmpp.packet.Packet;
import ru.tro.botframework4xmpp.extensions.ExtChannelExtension;

public class MessageGenerator {

	/**
	 * Генерирует пакет типа VkMessage по JID оправителя и получателя
	 * @param to
	 * @param from
	 * @param msg
	 * @return
	 */
	public static Message genMsg(JID to, JID from, String msg)
	{
		Message message = new Message();
		message.setType(Type.chat);
		message.setTo(to);
		message.setFrom(from);
		message.addChildElement("active", "http://jabber.org/protocol/chatstates");
		message.setBody(msg);
		
		return message;
	}

	/**
	 * Генерирует пакет типа VkMessage по имени оправителя и получателя
	 * @param to
	 * @param from
	 * @param msg
	 * @return
	 */
	public static Packet genMsg(String to, String from, String msg)
	{
		Message message = new Message();
		message.setType(Type.chat);
		message.setTo(to);
		message.setFrom(from);
		message.addChildElement("active", "http://jabber.org/protocol/chatstates");
		message.setBody(msg);
		
		return message;
	}

	public static Packet genExtLoginMsg(String login, String to, String from, String msg) {
		Packet message = genMsg(to, from, msg);
		message.addExtension(new ExtChannelExtension(login));

		return message;
	}
}
