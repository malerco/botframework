package ru.tro.botframework4xmpp.handlers;

import clientapi.okruclient.OkRuClient;
import clientapi.viberpublic.ViberPublicClient;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.types.Page;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.xmpp.packet.*;
import org.xmpp.packet.IQ.Type;
import ru.tro.botframework4xmpp.Server;
import ru.tro.botframework4xmpp.beans.ContactBean;
import ru.tro.botframework4xmpp.beans.UserBean;
import ru.tro.botframework4xmpp.config.Config;
import ru.tro.botframework4xmpp.database.DatabaseManager;
import ru.tro.botframework4xmpp.gateway.ChannelSwitcher;
import ru.tro.botframework4xmpp.singleton.Contacts;
import ru.tro.botframework4xmpp.singleton.Users;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ru.tro.botframework4xmpp.Server.CHANNEL.*;

/**
 * Класс обрабатывающий пакеты типа <query xmlns="jabber:iq:register"/>
 * @author TANT
 *
 */
public class RegisterHandler {
	private static final Logger _log = Logger.getLogger(RegisterHandler.class.getName());


	public static void process(Packet packet)
	{
		IQ _iq = (IQ) packet;
		Server.CHANNEL CHNL;
		try {
			CHNL = Server.CHANNEL.valueOf(packet.getTo().getDomain().split("\\.")[0]);
		} catch (Exception e) {
			_log.log(Level.WARNING, "cannot parse channel: " + packet.getTo());
			return;
		}
		switch (_iq.getType().name()) {
		case "get":
		{
			IQ iq = new IQ(Type.result, packet.getID());
			iq.setTo(packet.getFrom());
			iq.setFrom(packet.getTo());
			Element query = (Element) packet.getElement().element("query").detach();
			query.clearContent();

			boolean isNewUser = !Users.getInstance().containsJid(CHNL, packet.getFrom().toBareJID());

			Element form = DocumentHelper.createElement("x");
			form.addAttribute("type", "form");
			UserBean user = null;

			/**
        	* get auth url, obtaining code for get user access token
        	* Example: https://www.facebook.com/dialog/oauth?client_id=1835511946708008&redirect_uri=https%3A%2F%2Fmysender.im%2F&scope=public_profile%2Cmanage_pages
        	*/

			Element instructions = DocumentHelper.createElement("instructions");
			instructions.setText("Enter fields:");
			form.add(instructions);


            /*
                <field var =’evt. category ’
                    type =’list - single ’
                    label =’Event ␣ Category ’>
                    <validate xmlns =’http: // jabber .org / protocol /xdata - validate ’
                    datatype =’xs:string ’>
                    <open / >
                    </ validate >
                    <option ><value >holiday </ value ></ option >
                    <option ><value >reminder </ value ></ option >
                    <option ><value >appointment </ value ></ option >
                </ field >

                Element channel = DocumentHelper.createElement("field");
                channel.addAttribute("var", "channel");
                channel.addAttribute("type", "list-single");
                channel.addAttribute("label", "Channel: ");
                Element validate = DocumentHelper.createElement("validate");
                validate.addAttribute("xmlns", "http://jabber.org/protocol/xdata-validate");
                validate.addAttribute("datatype", "xs:string");
                Element open = DocumentHelper.createElement("open");
                validate.add(open);
                channel.add(validate);
                for (Server.CHANNEL ch : Server.CHANNEL.values()) {
                    Element option = DocumentHelper.createElement("option");
                    Element value = DocumentHelper.createElement("value");
                    value.setText(ch.name());
                    option.add(value);
                    channel.add(option);
                }
            */

			Element appid = DocumentHelper.createElement("field");
			appid.addAttribute("var", "appid");
			appid.addAttribute("type", "text-single");
			appid.addAttribute("label", "App ID: ");

			Element appsecret = DocumentHelper.createElement("field");
			appsecret.addAttribute("var", "appsecret");
			appsecret.addAttribute("type", "text-single");
			appsecret.addAttribute("label", "App secret: ");

			Element token = DocumentHelper.createElement("field");
			token.addAttribute("var", "token");
			token.addAttribute("type", "text-single");
			token.addAttribute("label", "Token: ");


            Element phone = DocumentHelper.createElement("field");
            phone.addAttribute("var", "phone");
            phone.addAttribute("type", "text-single");
            phone.addAttribute("label", "Phone: ");

			if (!isNewUser) {
				Element registered = DocumentHelper.createElement("registered");
				query.add(registered);
				user = Users.getInstance().findUserByJid(CHNL, packet.getFrom().toBareJID());
				user.setResource(packet.getFrom().getResource());

				Element value = DocumentHelper.createElement("value");
                value.setText(user.getAppId());
                appid.add(value);

				value = DocumentHelper.createElement("value");
				value.setText(user.getAppSecret());
				appsecret.add(value);

                value = DocumentHelper.createElement("value");
                value.setText(user.getAccessToken());
                token.add(value);

				Element unreg = DocumentHelper.createElement("field");
				unreg.addAttribute("var", "unregister");
				unreg.addAttribute("type", "boolean");
				unreg.addAttribute("label", "Remove your registration");
				value = DocumentHelper.createElement("value");
				value.setText("0");
				unreg.add(value);
				form.add(unreg);

			}
			switch (CHNL) {
				case facebook:
				case skype:
				case slack:
				case kik:
				case wechat:
					form.add(appid);
					form.add(appsecret);	
					break;
				case okru:
				case vk:
				case fb:
				case viberpublic:
				case telegram3:
					form.add(token);
					break;
                case telegram2:
                    form.add(phone);
                    break;
			}

			//few in one
			Element loginList = DocumentHelper.createElement("field");
			loginList.addAttribute("var", "loginlist");
			loginList.addAttribute("type", "list-single");
			loginList.addAttribute("label", "Logins: ");
			Element validate = DocumentHelper.createElement("validate");
			validate.addAttribute("xmlns", "http://jabber.org/protocol/xdata-validate");
			validate.addAttribute("datatype", "xs:string");
			Element open = DocumentHelper.createElement("open");
			validate.add(open);
			loginList.add(validate);
            if (user != null) {
                for (String p : Users.getInstance().getLoginValues(CHNL, user.getJid())) {
                    Element option = DocumentHelper.createElement("option");
                    Element value = DocumentHelper.createElement("value");
                    value.setText(p);
                    option.add(value);
                    loginList.add(option);
                }
            }

            form.add(loginList);
			query.add(form);

			iq.setChildElement(query);
			NameSpaceChanger.setNamespaces(query, DocumentHelper.createNamespace("", "jabber:iq:register"));
			NameSpaceChanger.setNamespaces(form, DocumentHelper.createNamespace("", "jabber:x:data"));

			ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(iq);
			break;
		}
		case "set":
		{
			String xtype = packet.getElement().element("query").element("x").attributeValue("type");
			
			if (xtype != null)
			{
				if (xtype.equals("submit")) {
					List fields = packet.getElement().element("query").element("x").elements();
					String appId = "-1";
					String appSecret = "-1";
					String token = "-1";
					String phone = "-1";
					String choosedLogin = null;
					boolean unreg = false;

					for (Object object : fields) {
						Element field = (Element) object;
						if (field.attribute("var") != null) {
						    switch (field.attribute("var").getText()) {
                                case "appid":
                                    appId = field.element("value").getText();
                                    break;
                                case "appsecret":
                                    appSecret = field.element("value").getText();
                                    break;
                                case "token":
                                    token = field.element("value").getText();
                                    break;
                                case "phone":
                                    phone = field.element("value").getText();
                                    break;
                                case "unregister":
                                    unreg = field.element("value").getText().equals("1");
                                    break;
                                case "loginlist":

                                    if (!unreg) {
                                        break;
                                    }

                                    choosedLogin = field.element("value") != null ? field.element("value").getText() : "";
                                    break;
                            }

						}
					}


					if (unreg && choosedLogin != null) {
						unregisterUser(CHNL, choosedLogin, packet);

                    } else if (Users.getInstance().findUserByLogin(CHNL, appId, token, phone) != null) {
                        Message msg = new Message();
                        msg.setType(org.xmpp.packet.Message.Type.error);
                        msg.setTo(packet.getFrom());
                        msg.setFrom(packet.getTo());
                        msg.setBody("This login is already used");
                        msg.setError(new PacketError(PacketError.Condition.internal_server_error));
                        ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(msg);

                    } else {
						registerUser(CHNL, packet.getFrom().toBareJID(), appId, appSecret, token, phone, packet);
					}
				}
			}
		}
		default:
			break;
		}
	}

	public static boolean registerUser(Server.CHANNEL CHNL, String jid, String appId, String appSecret, String token, String phone, Packet packet) {
		try {
			if (phone != null &&
					!phone.isEmpty() && CHNL == telegram2 && !phone.startsWith("+")) {
				phone = "+".concat(phone);
			}

			UserBean user = new UserBean(CHNL);
			user.setId(-1);
			user.setJid(jid);
			user.setResource("mychat");
			user.setChannel(CHNL);
			user.setAppId(appId);
			user.setAppSecret(appSecret);
			user.setAccessToken(token);
			user.setPhone(phone);

			//TODO инициализация апи

			//Add user to database
			if (CHNL == okru || CHNL == viberpublic) {
				try {
					if (CHNL == okru) {
						String webhook = Config.OKRU_WEBHOOK_ADDRESS + user.getBareJid();
						new OkRuClient(token).subscribe(webhook);
					} else if (CHNL == viberpublic) {
						String webhook = Config.VIBERPUBLIC_WEBHOOK_ADDRESS + user.getBareJid();
						new ViberPublicClient(token).subscribe(webhook);
					}
				} catch (IOException e) {
					if (packet != null) {
						Message msg = new Message();
						msg.setType(org.xmpp.packet.Message.Type.error);
						msg.setTo(packet.getFrom());
						msg.setFrom(packet.getTo());
						msg.setBody("Invalid Webhook URL");
						msg.setError(new PacketError(PacketError.Condition.bad_request));
						ChannelSwitcher.getInstance().getComponent(CHNL).sendPacket(msg);
					} else {
						e.printStackTrace();
					}
					e.printStackTrace();

					return false;
				}
			} else if (CHNL == fb) {
				FacebookClient facebookClient = new DefaultFacebookClient(user.getAccessToken(), Version.VERSION_2_9);
				Page page = facebookClient.fetchObject("me", Page.class);
				user.setChannelId(page.getId());

			}
			DatabaseManager.addNewUser(user);

			ArrayList<Packet> result = new ArrayList<>();
			Packet ro = RosterHandler.genRoster(user, CHNL.getWithDomain(), CHNL.name(), "Transports", "from");
			result.add(ro);
			result.add(PresenceHandler.genStatus(user, CHNL.getWithDomain(), org.xmpp.packet.Presence.Type.subscribe));
			result.add(PresenceHandler.genStatus(user, CHNL.getWithDomain(), null));

			if (packet != null) {
				IQ successful = new IQ(Type.result, packet.getID());
				successful.setTo(packet.getFrom());
				successful.setFrom(packet.getTo());
				result.add(successful);
			}

			user.setOnline(true);
			ChannelSwitcher.getInstance().getComponent(CHNL).sendPackets(result);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean unregisterUser(Server.CHANNEL CHNL, String choosedLogin, Packet packet) {
		try {

			UserBean user = Users.getInstance().findUserByLogin(CHNL, choosedLogin);

			user.setOnline(false);
			ArrayList<Packet> toRemove = new ArrayList<>();

			for (ContactBean cont : Contacts.getInstance().findAllByOwnerId(user)) {
				Packet ro = RosterHandler.genRoster(cont, CHNL.name(), "remove");
				toRemove.add(ro);
				toRemove.add(PresenceHandler.genStatus(user, cont.getJid(), org.xmpp.packet.Presence.Type.unavailable));
				toRemove.add(PresenceHandler.genStatus(user, cont.getJid(), org.xmpp.packet.Presence.Type.unsubscribed));
			}
			DatabaseManager.removeUserById(user);
			ChannelSwitcher.getInstance().getComponent(CHNL).sendPackets(toRemove);

			if (packet != null) {
				ArrayList<Packet> toSend = new ArrayList<>();
				//<iq from='***.openfire.mysender.ru' id='purple5e95b7d4'
				// to='***@openfire.mysender.ru/2kdvqh0rz0'
				// type='result'/>
				IQ successful = new IQ(Type.result, packet.getID());
				successful.setTo(packet.getFrom());
				successful.setFrom(packet.getTo());
				toSend.add(successful);

				//<presence from='***.openfire.mysender.ru'
				// to='***@openfire.mysender.ru'
				// type='unsubscribe'/>
				Presence unsubscribe = new Presence();
				unsubscribe.setFrom(packet.getTo());
				unsubscribe.setTo(packet.getFrom());
				unsubscribe.setType(Presence.Type.unsubscribe);
				toSend.add(unsubscribe);


				//<presence from='***.openfire.mysender.ru'
				// to='***@openfire.mysender.ru'
				// type='unsubscribed'/>
				Presence unsubscribed = new Presence();
				unsubscribed.setFrom(packet.getTo());
				unsubscribed.setTo(packet.getFrom());
				unsubscribed.setType(Presence.Type.unsubscribed);
				toSend.add(unsubscribed);

				//send...
				ChannelSwitcher.getInstance().getComponent(CHNL).sendPackets(toSend);
			}

			if (CHNL == okru) {
				String webhook = Config.OKRU_WEBHOOK_ADDRESS + user.getBareJid();
				try {
					new OkRuClient(user.getAccessToken()).unsubscribe(webhook);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (CHNL == viberpublic) {
				try {
					new ViberPublicClient(user.getAccessToken()).unsubscribe();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (CHNL == telegram2) {
				TelegramHandler.deleteTelegramAuth(user.getPhone());
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
